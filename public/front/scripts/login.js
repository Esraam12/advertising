var Login = function () {

    var init = function () {
        handleLogin();
    };

   
    var handleLogin = function () {
        $('#loginForm').validate({
            rules: {
                username: {
                    required: true,
                },
               password: {
                    required: true,
                }
            },
            //messages: lang.messages,
            highlight: function (element) { // hightlight error inputs
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element) {
                $(element).removeClass('is-invalid').addClass('is-valid');
                $(element).closest('.form-group').find('.invalid-feedback').html('').css('display', 'none');
            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.invalid-feedback').html($(error).html()).css('display', 'block');
            }
        });
        

        $('#loginForm .submit-form').click(function () {
            if ($('#loginForm').validate().form()) {
                $('#loginForm .submit-form').prop('disabled', true);
                $('#loginForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">'+lang.loading+'</span>');
                setTimeout(function () {
                    $('#loginForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#loginForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#loginForm').validate().form()) {
                    $('#loginForm .submit-form').prop('disabled', true);
                    $('#loginForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">' + lang.loading + '</span>');
                    setTimeout(function () {
                        $('#loginForm').submit();
                    }, 1000);
                }
                return false;
            }
        });
        
        $('#loginForm').submit(function () {
            var action = config.url + '/login';
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#loginForm .submit-form').prop('disabled', false);
                    $('#loginForm .submit-form').html(lang.login);

                    if (data.type == 'success') {
                        window.location.href = config.url+'/';

                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').addClass('is-invalid');
                                $('[name="' + i + '"]').closest('.form-group').find('.invalid-feedback').html(message).css('display', 'block');
                            }
                        }
                        else{
                            $('#alertMessage').show();
                            $('#alertMessage #message').html(data.message);
                        }
                        
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#loginForm .submit-form').prop('disabled', false);
                    $('#loginForm .submit-form').html(lang.login);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });


            return false;

        })
    }
    
    

    return {
        init: function () {
            init();
        }
    };

}();
jQuery(document).ready(function () {
    Login.init();
});