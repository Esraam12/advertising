var Rates = function () {
var Page = 2;

    var init = function () {
        handleLoadmoreRates();
        handleScroll();
        handleSubmit();
    };
    var handleSubmit = function () {
        $('#rateForm').validate({
            rules: {
                rate: {
                    required: true
                }
            },
            //messages: lang.messages,
            highlight: function (element) { // hightlight error inputs
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element) {
                $(element).removeClass('is-invalid').addClass('is-valid');
                $(element).closest('.form-group').find('.invalid-feedback').html('').css('display', 'none');
            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.invalid-feedback').html($(error).html()).css('display', 'block');
            }
        });
        

        $('#rateForm .submit-form').click(function () {
            if ($('#rateForm').validate().form()) {
                $('#rateForm .submit-form').prop('disabled', true);
                $('#rateForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#rateForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#rateForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#rateForm').validate().form()) {
                    $('#rateForm .submit-form').prop('disabled', true);
                    $('#rateForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#rateForm').submit();
                    }, 1000);
                }
                return false;
            }
        });
        
        $('#rateForm').submit(function () {
            var action = config.url + '/rate';
            var formData = new FormData($(this)[0]);
             formData.append('entity_id', new_config.entity_id);
             formData.append('entity_type', new_config.entity_type);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $('#rateForm .submit-form').prop('disabled', false);
                    $('#rateForm .submit-form').html(lang.send);

                    if (data.type == 'success') {
                        My.toast(data.data.message);
                       $('#comments-section').prepend(data.data.rate);
                       $('#reply-section').fadeOut();
                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').addClass('is-invalid');
                                $('[name="' + i + '"]').closest('.form-group').find('.invalid-feedback').html(message).css('display', 'block');
                            }
                        }
                        else{
                            $('#alertMessage').show();
                            $('#alertMessage #message').html(data.message);
                        }
                        
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#rateForm .submit-form').prop('disabled', false);
                    $('#rateForm .submit-form').html(lang.send);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });


            return false;

        })
    }
     var handleScroll = function () {
         $(window).on("scroll", function () {
             if ($(window).scrollTop() + $(window).height() > $(document).height() - 600) {
                 if ($('#load-more-rates-button').css('display') == 'block') {
                     $('#load-more-rates-button').trigger("click");
                 }
             }
         })
     }
    
    var handleLoadmoreRates = function () {
                 $('#load-more-rates-button').on('click', function () {
                     $(this).prop('disabled', true);
                     $(this).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">' + lang.loading + '</span>');
                    var query = $.param(new_config);
                    var action = config.url+ '/get_rates?page=' + Page + '&' + query;
                     
                     $.ajax({
                         url: action,
                         async: false,
                         cache: false,
                         contentType: false,
                         processData: false,
                         success: function (data) {
                             $('#load-more-rates-button').prop('disabled', false);
                             $('#load-more-rates-button').html(lang.load_more);
                             if (data.type == 'success') {
                                 if (data.data.rates != "") {
                                     $('#comments-section').append(data.data.rates);
                                     Page++;
                                 } else {
                                     $('#load-more-rates-button').css('display', 'none');
                                 }

                             }
                         },
                         error: function (xhr, textStatus, errorThrown) {
                             $('#load-more-rates-button').prop('disabled', false);
                             $('#load-more-rates-button').html(lang.load_more);
                             My.ajax_error_message(xhr);
                         },
                         dataType: "json",
                         type: "GET"
                     });
                     return false;

                 })
       

    }

    return {
        init: function () {
            init();
        }
    };

}();
jQuery(document).ready(function () {
    Rates.init();
});