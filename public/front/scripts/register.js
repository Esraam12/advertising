var Register = function () {

    var init = function () {
        handleSubmit();
        handleChangeCountry();
        handleChangeCurrency();
    };

     var handleChangeCountry = function () {
         $('#country').on('change', function () {
             var country = $(this).val();
             $('#city').html("");
             $('#city').html('<option selected value="">' + lang.choose + '</option>');
             cities = "";
             if (country) {
                 $.get('' + config.url + '/getLocations/' + country, function (data) {
                     if (data.data.length != 0) {
                         for (var x = 0; x < data.data.length; x++) {
                             var item = data.data[x];
                             cities += '<option value="' + item.id + '">' + item.title + '</option>';
                         }
                         $('#city').append(cities);
                     }
                 }, "json");
             }
         })
    }


    var handleSubmit = function () {
        $('#registerForm').validate({
            rules: {
                name: {
                    required: true,
                },
                email: {
                    required: true,
                    email:true
                },
                age: {
                    required: true,
                },
                gender: {
                    required: true,
                },
                dial_code: {
                    required: true,
                },
                mobile: {
                    required: true,
                },
                country: {
                    required: true,
                },
                city: {
                    required: true,
                }, 
                password: {
                    required: true,
                }

            },
            //messages: lang.messages,
            highlight: function (element) { // hightlight error inputs
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element) {
                $(element).removeClass('is-invalid').addClass('is-valid');
                $(element).closest('.form-group').find('.invalid-feedback').html('').css('display', 'none');
            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.invalid-feedback').html($(error).html()).css('display', 'block');
            }
        });
        

        $('#registerForm .submit-form').click(function () {
            if ($('#registerForm').validate().form()) {
                $('#registerForm .submit-form').prop('disabled', true);
                $('#registerForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">'+lang.loading+'</span>');
                setTimeout(function () {
                    $('#registerForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#registerForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#registerForm').validate().form()) {
                    $('#registerForm .submit-form').prop('disabled', true);
                    $('#registerForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">'+lang.loading+'</span>');
                    setTimeout(function () {
                        $('#registerForm').submit();
                    }, 1000);
                }
                return false;
            }
        });
        
        $('#registerForm').submit(function () {
            var action = config.url + '/register';
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    $('#registerForm .submit-form').prop('disabled', false);
                    $('#registerForm .submit-form').html(lang.register);

                    if (data.type == 'success') {
                        My.toast(data.message);
                        window.location.href = config.url+'/login';

                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').addClass('is-invalid');
                                $('[name="' + i + '"]').closest('.form-group').find('.invalid-feedback').html(message).css('display', 'block');

                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#registerForm .submit-form').prop('disabled', false);
                    $('#registerForm .submit-form').html(lang.register);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });


            return false;

        })




    }

    return {
        init: function () {
            init();
        }
    };

}();
jQuery(document).ready(function () {
    Register.init();
});