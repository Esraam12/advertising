var Profile = function () {

    var init = function () {
        handleSubmit();
        handleChangeCountry();
        My.readImageMulti('image');
    };
     var handleChangeCountry = function () {
         $('#country').on('change', function () {
             var country = $(this).val();
             $('#city').html("");
             $('#city').html('<option selected value="">' + lang.choose + '</option>');
             cities = "";
             if (country) {
                 $.get('' + config.url + '/getLocations/' + country, function (data) {
                     if (data.data.length != 0) {
                         for (var x = 0; x < data.data.length; x++) {
                             var item = data.data[x];
                             cities += '<option value="' + item.id + '">' + item.title + '</option>';
                         }
                         $('#city').append(cities);
                     }
                 }, "json");
             }
         })
    }

    var handleSubmit = function () {
        $('#editProfileForm').validate({
            rules: {
                name: {
                    required: true,
                },
                email: {
                    required: true,
                    email:true
                },
                age: {
                    required: true,
                },
                gender: {
                    required: true,
                },
                country: {
                    required: true,
                },
                city: {
                    required: true,
                }
               

            },
            //messages: lang.messages,
            highlight: function (element) { // hightlight error inputs
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element) {
                $(element).removeClass('is-invalid').addClass('is-valid');
                $(element).closest('.form-group').find('.invalid-feedback').html('').css('display', 'none');
            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.invalid-feedback').html($(error).html()).css('display', 'block');
            }
        });
        

        $('#editProfileForm .submit-form').click(function () {
            if ($('#editProfileForm').validate().form()) {
                $('#editProfileForm .submit-form').prop('disabled', true);
                $('#editProfileForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#editProfileForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#editProfileForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#editProfileForm').validate().form()) {
                    $('#editProfileForm .submit-form').prop('disabled', true);
                    $('#editProfileForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#editProfileForm').submit();
                    }, 1000);
                }
                return false;
            }
        });
        
        $('#editProfileForm').submit(function () {
            var action = config.url + '/profile/edit';
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    $('#editProfileForm .submit-form').prop('disabled', false);
                    $('#editProfileForm .submit-form').html(lang.edit);

                    if (data.type == 'success') {
                        My.toast(data.message);
                        window.location.reload();

                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                $('[name="' + i + '"]').addClass('is-invalid');
                                $('[name="' + i + '"]').closest('.form-group').find('.invalid-feedback').html(message).css('display', 'block');

                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#editProfileForm .submit-form').prop('disabled', false);
                    $('#editProfileForm .submit-form').html(lang.save);
                    console.log(xhr);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });


            return false;

        })




    }

    return {
        init: function () {
            init();
        }
    };

}();
jQuery(document).ready(function () {
    Profile.init();
});