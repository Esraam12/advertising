var directionRtl = true;
if (($('html').attr('lang')) == 'en') {
    directionRtl = false;
}

new WOW().init();

/*$('.fa-bookmark').click(
    function(){
        $(this).toggleClass('bookmarked')
    }
)*/

document.getElementById("uploadBtn").onchange = function () {
    document.getElementById("uploadFile").value = this.value.replace("C:\\fakepath\\", "");
};

document.getElementById("uploadBtn2").onchange = function () {
    document.getElementById("uploadFile2").value = this.value.replace("C:\\fakepath\\", "");
};

$(".artclesSlider").slick({
    dots: true,
    arrows: false,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    rtl: directionRtl,
    responsive: [{
        breakpoint: 992,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            dots: true
        }
    }, {
        breakpoint: 576,
        settings: {
            slidesToScroll: 1,
            dots: true,
            vertical: true,
            slidesToShow: 1,
        }
    }
]
})