$(document).ready(function () {

 
    $(".artclesSlider").slick({
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        rtl: directionRtl,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 576,
            settings: {
                slidesToScroll: 1,
                dots: true,
                vertical: true,
                slidesToShow: 1,
            }
        }
        ]
    });

    $('.pay-btn').click(function () {
        $('.hidden-area').removeClass("hidden");
    })

    $('.other-btn').click(function () {
        $('.hidden-area').addClass("hidden");
    })

    $('#dial_code').wSelect();




    var directionRtl = true;
    if (config.lang_code == 'en') {
        directionRtl = false;
    }
    $(".img-slider").slick({
        dots: false,
        arrows: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        rtl: directionRtl,
        autoplay: true
    });
    // for multi level select in the nav bar [make the head to the select clickable even if in the mobile screen]
    $('.navbar li.nav-item > .dropdown > a[href]').click(function () {
        var dropdown = $(this).next('.dropdown-menu');
        if (dropdown.length == 0 || $(dropdown).css('display') !== 'none') {
            if (this.href) {
                location.href = this.href;
            }
        }
    });
    // to hide logo in case of nav toggle
    $("#navbar-toggler").click(function () {
         var $this = $(this);
         var clickCount = ($this.data("click-count") || 0) + 1;
         $this.data("click-count", clickCount);
         if (clickCount % 2 == 0) {
             $("#navbar-brand").show();
         } else {
             $("#navbar-brand").hide();
         }

     });
  

});
