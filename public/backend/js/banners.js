var Banner_grid;

var Banner = function () {

    var init = function () {
        handleRecords();
        handleChangeCountry(); 
        handleSubmit();
         My.readImageMulti('image');

      
    };


    var handleRecords = function () {

        Banner_grid = $('.dataTable').dataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": config.admin_url + "/banners/data",
                "type": "POST",
                data: {
                    _token: $('input[name="_token"]').val()
                },
            },
            "columns": [
                {
                    "data": "title",
                    "name": "banner_translations.title"
                },
                {
                    "data": "image",
                    "name": "banners.image",
                    orderable: false,
                    searchable: false
                },
                {
                    "data": "active",
                    "name": "banners.active"
                },
                {
                    "data": "duration",
                    "name": "banners.duration"
                },
                {
                    "data": "options",
                    orderable: false,
                    searchable: false
                }
            ],
            "order": [
                [1, "asc"]
            ],

            "oLanguage": {
                "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json'
            }

        });
    }
    var handleChangeCountry = function () {
        $('#country').on('change', function () {
            var country = $(this).val();
            $('#city').html("");
            $('#city').html('<option selected value="">' + lang.choose + '</option>');
            cities = "";
            if (country) {
                $.get('' + config.admin_url + '/getLocations/' + country, function (data) {
                    if (data.data.length != 0) {
                        for (var x = 0; x < data.data.length; x++) {
                            var item = data.data[x];
                            cities += '<option value="' + item.id + '">' + item.title + '</option>';
                        }
                        $('#city').append(cities);
                    }
                }, "json");
            }
        })
   }

    var handleSubmit = function () {


        $('#addEditBannerForm').validate({
            rules: {

                'image': {
                    required: true,
                    extension: "png|jpeg|jpg|gif",
                },
                active: {
                    required: true,
                },
                this_order: {
                    required: true,
                },
                duration: {
                    required: true,
                },
                frequency:{
                    required: true,
                }
               
            },
            //messages: lang.messages,
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });
        
     
          var langs = JSON.parse(config.languages);
          for (var x = 0; x < langs.length; x++) {
              var ele = "input[name='title[" + langs[x] + "]']";
              $(ele).rules('add', {
                  required: true,
              });
          }

        if ($('#id').val() != 0) {
            $('#image').rules('remove');
        }


        $('#addEditBannerForm .submit-form').click(function () {

            if ($('#addEditBannerForm').validate().form()) {
                $('#addEditBannerForm .submit-form').prop('disabled', true);
                $('#addEditBannerForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#addEditBannerForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#addEditBannerForm input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#addEditBannerForm').validate().form()) {
                    $('#addEditBannerForm .submit-form').prop('disabled', true);
                    $('#addEditBannerForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function () {
                        $('#addEditBannerForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#addEditBannerForm').submit(function () {
            var id = $('#id').val();
            var action = config.admin_url + '/banners';
            var formData = new FormData($(this)[0]);
            if (id != 0) {
                formData.append('_method', 'PATCH');
                action = config.admin_url + '/banners/' + id;
            }
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    $('#addEditBannerForm .submit-form').prop('disabled', false);
                    $('#addEditBannerForm .submit-form').html(lang.save);

                    if (data.type == 'success') {

                        if (id == 0) {
                            Banner.empty();
                        } 
                         My.toast(data.message);

                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                if (i.startsWith('title')) {
                                    var key_arr = i.split('.');
                                    var key_text = key_arr[0] + '[' + key_arr[1] + ']';
                                    i = key_text;
                                }
                                $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $('#addEditBannerForm .submit-form').prop('disabled', false);
                    $('#addEditBannerForm .submit-form').html(lang.save);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });


            return false;

        })




    }

    return {
        init: function () {
            init();
        },
        delete: function (t) {

            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/banners/' + id,
                data: {
                    _method: 'DELETE',
                    _token: $('input[name="_token"]').val()
                },
                success: function (data) {
                    Banner_grid.api().ajax.reload();
                }
            });

        },

        error_message: function (message) {
            $.alert({
                title: lang.error,
                content: message,
                type: 'red',
                typeAnimated: true,
                buttons: {
                    tryAgain: {
                        text: lang.try_again,
                        btnClass: 'btn-red',
                        action: function () {}
                    }
                }
            });
        },
        empty: function () {
            $('#id').val(0);
            $('#active').find('option').eq(0).prop('selected', true);
            $('input[type="checkbox"]').prop('checked', false);
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
             $('.image_box').html('<img src="' + config.url + '/no-image.png" width="100" height="80" class="image" />');
             $('#image').val('');
            My.emptyForm();
        }
    };

}();
jQuery(document).ready(function () {
    Banner.init();
});