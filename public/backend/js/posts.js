var page = 2;
var voice;
var Posts = function () {

    var init = function () {
        //handleRecords();
        handleSearch();
        if (new_config.id != undefined) {
            id = new_config.id;
            handleScrollUp();
            handleSubmit();
            handleAttachmentTypeChange();
            for (let i = 0; i < 5; i++) {
                My.readImageMultiWithRemove('image_' + i + ''); 
            }
            
        }
        
    };

    var handleAttachmentTypeChange = function(){
        $('#attachments_type').on('change', function (e) {
            var type = e.target.value;
            if (type == 1) {
                $('#post_images').show();
                $('#post_files').hide();
                $('#post_voice').hide(); 
                 $('input[name="images[0]"]').rules('add', {required: true});
                 $('input[name = "files[]"]').rules('remove');
                 $('input[name = voice]').rules('remove');
            } else if (type == 2){
                $('#post_files').show();
                $('#post_images').hide();
                $('#post_voice').hide(); 
                 $('input[name="images[0]"]').rules('remove');
                 $('input[name = "files[]"]').rules('add', {required: true});
                 $('input[name = voice]').rules('remove');
            }
            else if (type == 3) {
                $('#post_voice').show(); 
                $('#post_images').hide();
                $('#post_files').hide();
                $('input[name="images[0]"]').rules('remove');
                 $('input[name = "files[]"]').rules('remove');
                 $('input[name = "voice"]').rules('add', {required: true});
                
            }else{
                $('#post_images').hide();
                $('#post_files').hide();
                $('#post_voice').hide(); 
                 $('input[name="images[0]"]').rules('remove');
                 $('input[name = "files[]"]').rules('remove');
                 $('input[name = voice]').rules('remove');
            }
        });
    }

    var handleScrollUp = function(){
        $("#post_details").scrollTop($("#post_details")[0].scrollHeight);

        // Assign scroll function to chatBox DIV
        $('#post_details').scroll(function () {
            if ($('#post_details').scrollTop() == 0) {
                // Display AJAX loader animation
                $('#loader').show();
                //Simulate server delay;
                setTimeout(function () {
                    // Simulate retrieving 4 messages
                   $.ajax({
                       url: config.admin_url + '/posts/'+id+'?page=' + page,
                       success: function (data) {
                           $('#loader').hide();
                           if (data.data.details != "") {
                                page++;
                                $('#post_details').prepend(data.data.details);
                                // Reset scroll
                                $('#post_details').scrollTop(100);
                           }
                          
                       },
                       error: function (xhr, textStatus, errorThrown) {
                           My.ajax_error_message(xhr);
                       },
                       dataType: "json"

                   });
                   // Hide loader on success
                    
                }, 780);
            }
        });
    }

   
    var handleSearch = function () {
        $('.btn-search').on('click', function () {
            var data = $("#PostsSearchForm").serializeArray();
            var url = config.admin_url + "/posts";
            var params = {};
            $.each(data, function (i, field) {
                var name = field.name;
                var value = field.value;
                if (value) {
                    params[name] = value
                }
            });
            var query = $.param(params);
            if (!jQuery.isEmptyObject(params)) {
                url += '?' + query;
            }
            window.location.href = url;
            return false;
        })
    }

  var handleRecords = function(){
       $('ul.pagination').hide();
       
        $('.infinite-scroll').jscroll({
               autoTrigger: true,
               loadingHtml: '<img class="center-block" src="'+config.url+'/public/backend/images/loading.gif" alt="Loading..." />',
               padding: 0,
               nextSelector: '.pagination li.active + li a',
               contentSelector: 'div.infinite-scroll',
               callback: function () {
                   $('ul.pagination').remove();
               }
        });
       
  }
   
    var handleSubmit = function() {

        $('#addPostsForm').validate({
            rules: {
                ignore: [],
                text: {
                    required: true
                 }
            },
            //messages: lang.messages,
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function(error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });


        $('#addPostsForm .submit-form').click(function() {

            if ($('#addPostsForm').validate().form()) {
                $('#addPostsForm .submit-form').prop('disabled', true);
                $('#addPostsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function() {
                    $('#addPostsForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#addPostsForm input').keypress(function(e) {
            if (e.which == 13) {
                if ($('#addPostsForm').validate().form()) {
                    $('#addPostsForm .submit-form').prop('disabled', true);
                    $('#addPostsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function() {
                        $('#addPostsForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#addPostsForm').submit(function() {
            var id = $('#id').val();
            var action = config.admin_url + '/specialist_posts';
            var formData = new FormData($(this)[0]);
             $.ajax({
                 url: action,
                 data: formData,
                 async: false,
                 cache: false,
                 contentType: false,
                 processData: false,
                 success: function (data) {
                     console.log(data);
                     $('#addPostsForm .submit-form').prop('disabled', false);
                     $('#addPostsForm .submit-form').html(lang.save);

                     if (data.type == 'success') {
                         My.toast(data.message);
                         window.location.reload();
                     } else {
                         if (typeof data.errors !== 'undefined') {
                             for (i in data.errors) {
                                 var message = data.errors[i];
                                 $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                 $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                             }
                         }
                     }
                 },
                 error: function (xhr, textStatus, errorThrown) {
                     $('#addPostsForm .submit-form').prop('disabled', false);
                     $('#addPostsForm .submit-form').html(lang.save);
                     My.ajax_error_message(xhr);
                 },
                 dataType: "json",
                 type: "POST"
             });
            /*
            var action = config.admin_url + '/specialist_posts';
            var formData = new FormData($(this)[0]);
            if ($('#attachments_type').val() == 3) {
                Fr.voice.exportMP3(function (url) {
                    formData.append("voice", url);
                      $.ajax({
                          url: action,
                          data: formData,
                          async: false,
                          cache: false,
                          contentType: false,
                          processData: false,
                          success: function (data) {
                              console.log(data);
                              $('#addPostsForm .submit-form').prop('disabled', false);
                              $('#addPostsForm .submit-form').html(lang.save);

                              if (data.type == 'success') {
                                  My.toast(data.message);
                                  window.location.reload();
                              } else {
                                  if (typeof data.errors !== 'undefined') {
                                      for (i in data.errors) {
                                           if (i.startsWith('images')) {
                                               var key_arr = i.split('.');
                                               var key_text = key_arr[0] + '[' + key_arr[1] + ']';
                                               i = key_text;
                                           }
                                          var message = data.errors[i];
                                          $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                          $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                                      }
                                  }
                              }
                          },
                          error: function (xhr, textStatus, errorThrown) {
                              $('#addPostsForm .submit-form').prop('disabled', false);
                              $('#addPostsForm .submit-form').html(lang.save);
                              My.ajax_error_message(xhr);
                          },
                          dataType: "json",
                          type: "POST"
                      });
                }, "base64");
                //var voice = config.voice;
                
            }else{
                $.ajax({
                    url: action,
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        console.log(data);
                        $('#addPostsForm .submit-form').prop('disabled', false);
                        $('#addPostsForm .submit-form').html(lang.save);

                        if (data.type == 'success') {
                            My.toast(data.message);
                            window.location.reload();
                        } else {
                            if (typeof data.errors !== 'undefined') {
                                for (i in data.errors) {
                                    var message = data.errors[i];
                                    $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                    $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                                }
                            }
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        $('#addPostsForm .submit-form').prop('disabled', false);
                        $('#addPostsForm .submit-form').html(lang.save);
                        My.ajax_error_message(xhr);
                    },
                    dataType: "json",
                    type: "POST"
                });
            }
            
            */


            return false;

        })




    }
   
   



   

    return {
        init: function () {
            init();
        },
        delete: function (t) {

            var id = $(t).attr("data-id");
            var type = $(t).attr("data-type");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/posts/' + id,
                data: {
                    _method: 'DELETE',
                    _token: $('input[name="_token"]').val(),
                    type: type
                },
                success: function (data) {
                    $(t).closest('.timeline-item').fadeOut("slow");
                }
            });

        },
        status: function (t) {
              var post_id = $(t).data("id");
              $(t).prop('disabled', true);
              $(t).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');

              $.ajax({
                  url: config.admin_url + '/posts/status/' + post_id,
                  success: function (data) {
                      $(t).prop('disabled', false);
                      if ($(t).hasClass("active")) {
                          $(t).addClass('not_active').removeClass('active');
                          $(t).html(lang.active);

                      } else {
                          $(t).addClass('active').removeClass('not_active');
                          $(t).html(lang.not_active);
                      }
                      My.toast(data.message);
                  },
                  error: function (xhr, textStatus, errorThrown) {
                      My.ajax_error_message(xhr);
                  },
              });

          },

        error_message: function (message) {
            $.alert({
                title: lang.error,
                content: message,
                type: 'red',
                typeAnimated: true,
                buttons: {
                    tryAgain: {
                        text: lang.try_again,
                        btnClass: 'btn-red',
                        action: function () {}
                    }
                }
            });
        },
        empty: function () {
            $('#id').val(0);
            $('#active').find('option').eq(0).prop('selected', true);
            $('#category_id').find('option').eq(0).prop('selected', true);
            $('#type').find('option').eq(0).prop('selected', true);
            $('input[type="checkbox"]').prop('checked', false);
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            My.emptyForm();
        }
    };

}();
jQuery(document).ready(function () {
    Posts.init();
});