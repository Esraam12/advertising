(function (g, f) {
    typeof exports === 'object' && typeof module !== 'undefined' ? f(exports) : typeof define === 'function' && define.amd ? define(['exports'], f) : (g = g || self, f(g.mp3EncoderWorker = {}));
}(this, (function (exports) {
    'use strict';
    var initMp3MediaEncoder = function (ref) {
        var vmsgWasmUrl = ref.vmsgWasmUrl;

        // from vmsg
        // Must be in sync with emcc settings!
        var TOTAL_STACK = 5 * 1024 * 1024;
        var TOTAL_MEMORY = 128 * 1024 * 1024;
        var WASM_PAGE_SIZE = 64 * 1024;
        var ctx = self;
        var memory = new WebAssembly.Memory({
            initial: TOTAL_MEMORY / WASM_PAGE_SIZE,
            maximum: TOTAL_MEMORY / WASM_PAGE_SIZE
        });
        var dynamicTop = TOTAL_STACK;
        var vmsg;
        var isRecording = false;
        var vmsgRef;
        var pcmLeft;
        var getWasmModuleFallback = function (url, imports) {
            return fetch(url)
                .then(function (response) {
                    return response.arrayBuffer();
                })
                .then(function (buffer) {
                    return WebAssembly.instantiate(buffer, imports);
                });
        };
        var getWasmModule = function (url, imports) {
            if (!WebAssembly.instantiateStreaming) {
                return getWasmModuleFallback(url, imports);
            }
            return WebAssembly.instantiateStreaming(fetch(url), imports).catch(function () {
                return getWasmModuleFallback(url, imports);
            });
        };
        var getVmsgImports = function () {
            var onExit = function () {
                ctx.postMessage({
                    type: 'ERROR',
                    error: 'internal'
                });
            };
            var sbrk = function (increment) {
                var oldDynamicTop = dynamicTop;
                dynamicTop += increment;
                return oldDynamicTop;
            };
            var env = {
                memory: memory,
                sbrk: sbrk,
                exit: onExit,
                pow: Math.pow,
                powf: Math.pow,
                exp: Math.exp,
                sqrtf: Math.sqrt,
                cos: Math.cos,
                log: Math.log,
                sin: Math.sin
            };
            return {
                env: env
            };
        };
        var onStartRecording = function (config) {
            isRecording = true;
            vmsgRef = vmsg.vmsg_init(config.sampleRate);
            if (!vmsgRef || !vmsg) {
                throw new Error('init_failed');
            }
            var pcmLeftRef = new Uint32Array(memory.buffer, vmsgRef, 1)[0];
            pcmLeft = new Float32Array(memory.buffer, pcmLeftRef);
        };
        var onStopRecording = function () {
            isRecording = false;
            if (vmsg.vmsg_flush(vmsgRef) < 0) {
                throw new Error('flush_failed');
            }
            var mp3BytesRef = new Uint32Array(memory.buffer, vmsgRef + 4, 1)[0];
            var size = new Uint32Array(memory.buffer, vmsgRef + 8, 1)[0];
            var mp3Bytes = new Uint8Array(memory.buffer, mp3BytesRef, size);
            var blob = new Blob([mp3Bytes], {
                type: 'audio/mpeg'
            });
            vmsg.vmsg_free(vmsgRef);
            return blob;
        };
        var onDataReceived = function (data) {
            if (!isRecording) {
                return;
            }
            pcmLeft.set(data);
            var encodedBytesAmount = vmsg.vmsg_encode(vmsgRef, data.length);
            if (encodedBytesAmount < 0) {
                throw new Error('encoding_failed');
            }
        };
        ctx.addEventListener('message', function (event) {
            var message = event.data;
            try {
                switch (message.type) {
                    case 'START_RECORDING': {
                        onStartRecording(message.config);
                        ctx.postMessage({
                            type: 'WORKER_RECORDING'
                        });
                        break;
                    }
                    case 'DATA_AVAILABLE': {
                        onDataReceived(message.data);
                        break;
                    }
                    case 'STOP_RECORDING': {
                        var blob = onStopRecording();
                        ctx.postMessage({
                            type: 'BLOB_READY',
                            blob: blob
                        });
                        break;
                    }
                }
            } catch (err) {
                ctx.postMessage({
                    type: 'ERROR',
                    error: err.message
                });
            }
        });
        var imports = getVmsgImports();
        getWasmModule(vmsgWasmUrl, imports).then(function (wasm) {
            vmsg = wasm.instance.exports;
        });
    };
    exports.initMp3MediaEncoder = initMp3MediaEncoder;
    Object.defineProperty(exports, '__esModule', {
        value: true
    });
}))); //# sourceMappingURL=index.umd.js.map













self.mp3EncoderWorker.initMp3MediaEncoder({
    vmsgWasmUrl: 'vmsg.wasm'
});