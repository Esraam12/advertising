var Posts = function () {

    var init = function () {
        //handleRecords();
        handleSearch();
    };


    var handleSearch = function () {
        $('.btn-search').on('click', function () {
            var data = $("#PostsSearchForm").serializeArray();
            var url = config.admin_url + "/specialist_posts";
            var params = {};
            $.each(data, function (i, field) {
                var name = field.name;
                var value = field.value;
                if (value) {
                    params[name] = value
                }

            });
            var query = $.param(params);
            if (!jQuery.isEmptyObject(params)) {
                url += '?' + query;
            }
            window.location.href = url;
            return false;
        })
    }

  var handleRecords = function(){
       $('ul.pagination').hide();
     
        $('.infinite-scroll').jscroll({
               autoTrigger: true,
               loadingHtml: '<img class="center-block" src="'+config.url+'/public/backend/images/loading.gif" alt="Loading..." />',
               padding: 0,
               nextSelector: '.pagination li.active + li a',
               contentSelector: 'div.infinite-scroll',
               callback: function () {
                   $('ul.pagination').remove();
               }
        });
       
  }
   

   
   



   

    return {
        init: function () {
            init();
        },
        delete: function (t) {

            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/posts/' + id,
                data: {
                    _method: 'DELETE',
                    _token: $('input[name="_token"]').val()
                },
                success: function (data) {
                    $(t).closest('.timeline-item').fadeOut("slow");
                }
            });

        },
        status: function (t) {
              var post_id = $(t).data("id");
              $(t).prop('disabled', true);
              $(t).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');

              $.ajax({
                  url: config.admin_url + '/posts/status/' + post_id,
                  success: function (data) {
                      $(t).prop('disabled', false);
                      if ($(t).hasClass("active")) {
                          $(t).addClass('not_active').removeClass('active');
                          $(t).html(lang.active);

                      } else {
                          $(t).addClass('active').removeClass('not_active');
                          $(t).html(lang.not_active);
                      }
                  },
                  error: function (xhr, textStatus, errorThrown) {
                      My.ajax_error_message(xhr);
                  },
              });

          },

        error_message: function (message) {
            $.alert({
                title: lang.error,
                content: message,
                type: 'red',
                typeAnimated: true,
                buttons: {
                    tryAgain: {
                        text: lang.try_again,
                        btnClass: 'btn-red',
                        action: function () {}
                    }
                }
            });
        },
        empty: function () {
            $('#id').val(0);
            $('#active').find('option').eq(0).prop('selected', true);
            $('#category_id').find('option').eq(0).prop('selected', true);
            $('#type').find('option').eq(0).prop('selected', true);
            $('input[type="checkbox"]').prop('checked', false);
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            for (let i = 0; i < 6; i++) {
                $('.image_' + i + '_box').html('<img src="' + config.url + '/no-image.png" width="100" height="80" class="image_' + i + '" />');
                $('#image_' + i).val('');
            }
            $("#videos").tagsinput('removeAll');
            $('#categories').find('option').eq(0).prop('selected', true);
            $('#sub_categories').html('<option value="">' + lang.choose + '</option>');
            $('#specialists').html('<option value="">' + lang.choose + '</option>');
            $('#show_rates').prop('checked', false).change();
            My.emptyForm();
        }
    };

}();
jQuery(document).ready(function () {
    Posts.init();
});