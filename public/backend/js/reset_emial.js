var errors = [];
var ResetEmail = function () {
        var init = function () {
            handle_reset_email();
        }
        var handle_reset_email = function () {
            $("#resetEmailForm").validate({
                rules: {
                    email: {
                        required: true,
                        email:true
                    }
                },
                messages: {
                    email: {
                        required: lang.required,
                        email: lang.email_not_valid
                    }
                },
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    $(element).closest('.form-group').find('.help-block').html('');

                },
                errorPlacement: function (error, element) {
                    $(element).closest('.form-group').find('.help-block').html($(error).html());
                }

            });
            $('.submit-form').click(function () {
                if ($('#resetEmailForm').validate().form()) {
                    $('#resetEmailForm .submit-form').prop('disabled', true);
                    $('#resetEmailForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                     setTimeout(function () {
                       $('#resetEmailForm').submit();
                    }, 1000);
                }
                return false;
            });

            $('#resetEmailForm input').keypress(function (e) {
                if (e.which == 13) {
                    if ($('#resetEmailForm').validate().form()) {
                        $('#resetEmailForm .submit-form').prop('disabled', true);
                        $('##resetEmailForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                        setTimeout(function () {
                           $('#resetEmailForm').submit();
                        }, 1000);
                    }
                    return false;
                }
            });
          
            $('#resetEmailForm').submit(function () {
                var action = config.admin_url + '/password/email';
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: action,
                    data: formData,
                    dataType: "json",
                    type: "POST",
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data)
                    {
                        $('#resetEmailForm .submit-form').prop('disabled', false);
                        $('#resetEmailForm .submit-form').html(lang.send_reset_link);
                        
                        if (data.type == 'success') {
                            $('#success-message').html(data.message).fadeIn(400).delay(3000).fadeOut(400);
                        } else {
                            if (typeof data.errors !== 'undefined') {
                                for (i in data.errors)
                                {
                                    var message = data.errors[i];
                                   $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                   $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                                }
                            }
                            if (typeof data.message !== 'undefined') {
                                $('#alert-message').html(data.message).fadeIn(400).delay(3000).fadeOut(400);
                            }
                        }


                    },
                    error: function (xhr, textStatus, errorThrown) {
                        $('#resetEmailForm .submit-form').prop('disabled', false);
                        $('#resetEmailForm .submit-form').html(lang.send_reset_link);
                        My.ajax_error_message(xhr);
   
                    },
                });

                return false;
            });

        }

        return {
            init: function () {
                init();
            }
        }

    }();

    jQuery(document).ready(function () {
        ResetEmail.init();
    });


