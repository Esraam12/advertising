var errors = [];
var Sidebar = function () {
        var init = function () {
            handle_sidebar_changes();
        }
        var handle_sidebar_changes = function () {
            
           $('.page-sidebar-wrapper .nav-link').click(function(e){
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            data = [];
            e.preventDefault();

            dest = $(this).attr('href');
            if ($(this).parent().parent().hasClass('sub-menu')) {
                item = $(this).parent().attr('data-name')
                data.push(item)
                item = $(this).parent().parent().attr('data-name')
                data.push(item)
                item = $(this).parent().parent().parent().attr('data-name');
                data.push(item);
            }else if($(this).parent().parent().hasClass('page-sidebar-menu') && $(this).attr('href') !== "javascript:;"){
               item = $(this).parent().attr('data-name');
               data.push($(this).parent().attr('data-name'));
            }

            formData = JSON.stringify(data);
            action = config.admin_url + '/change_sidebar_state';
            $.ajax({
                type: "POST",
                url: action,
                data: formData,
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                        if (data.type == 'success') {
                           window.location.href = dest;
                        } else {
                            console.log('here');
                        }
                    }
                ,error: function (xhr, textStatus, errorThrown) {   
                    My.ajax_error_message(xhr);
                },
            });
          });
                
        }

        return {
            init: function () {
                init();
            }
        }

    }();

    jQuery(document).ready(function () {
        Sidebar.init();
    });


