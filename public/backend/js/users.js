var Users_grid;
var Subscriptions_grid;

var Users = function() {

    var init = function() {
        handleRecords();
        handleSubmit();
        handleChangeCountry();  
        My.readImageMulti('image');
    };
    var handleChangeCountry = function () {
        $('#country').on('change', function () {
            var country = $(this).val();
            $('#city').html("");
            $('#city').html('<option selected value="">' + lang.choose + '</option>');
            cities = "";
            if (country) {
                $.get('' + config.admin_url + '/getLocations/' + country, function (data) {
                    if (data.data.length != 0) {
                        for (var x = 0; x < data.data.length; x++) {
                            var item = data.data[x];
                            cities += '<option value="' + item.id + '">' + item.title + '</option>';
                        }
                        $('#city').append(cities);
                    }
                }, "json");
            }
        })
   }
 
   var handleSubmit = function () {
    $('#addEditUsersForm').validate({
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email:true
            },
            age: {
                required: true,
            },
            gender: {
                required: true,
            },
            country: {
                required: true,
            },
            city: {
                required: true,
            }

        },
        //messages: lang.messages,
        highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);
        },
        errorPlacement: function (error, element) {
            $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
        }
    });
    
    $('#addEditUsersForm .submit-form').click(function () {
        if ($('#addEditUsersForm').validate().form()) {
            $('#addEditUsersForm .submit-form').prop('disabled', true);
            $('#addEditUsersForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
            setTimeout(function () {
                $('#addEditUsersForm').submit();
            }, 1000);
        }
        return false;
    });
    $('#addEditUsersForm input').keypress(function (e) {
        if (e.which == 13) {
            if ($('#addEditUsersForm').validate().form()) {
                $('#addEditUsersForm .submit-form').prop('disabled', true);
                $('#addEditUsersForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function () {
                    $('#addEditUsersForm').submit();
                }, 1000);
            }
            return false;
        }
    });
    
    $('#addEditUsersForm').submit(function () {
        var id = $('#id').val();
      
        var action = config.admin_url + '/users';
        var formData = new FormData($(this)[0]);
        if (id != 0) {
            formData.append('_method', 'PATCH');
            action = config.admin_url + '/users/' + id;
        }
        $.ajax({
            url: action,
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data);
                $('#addEditUsersForm .submit-form').prop('disabled', false);
                $('#addEditUsersForm .submit-form').html(lang.save);

                if (data.type == 'success') {
                    My.toast(data.message);
                    if (id == 0) {
                        Users.empty();
                    }

                } else {
                    if (typeof data.errors !== 'undefined') {
                        for (i in data.errors) {
                            var message = data.errors[i];
                            $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                            $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                        }
                    }
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                $('#addEditUsersForm .submit-form').prop('disabled', false);
                $('#addEditUsersForm .submit-form').html(lang.register);
                My.ajax_error_message(xhr);
            },
            dataType: "json",
            type: "POST"
        });


        return false;

    })
}

    
    var handleRecords = function() {

        Users_grid = $('.dataTable').dataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": config.admin_url + "/users/data",
                "type": "POST",
                data: { _token: $('input[name="_token"]').val() },
            },
            "columns": [
                {"data": "username"},
                {"data": "email"},
                { "data": "image"},
                { "data": "gender" },
                { "data": "active"},
                { "data": "options", orderable: false, searchable: false }
            ],
            "order": [
                [0, "desc"]
            ],
            "columnDefs": [
                { "className": "dt-center", "targets": "_all" }
            ],
            "oLanguage": { "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json' }

        });
    }
  
    return {
        init: function() {
            init();
        },
      
     
       status: function(t) {
            var user_id = $(t).data("id"); 
            $(t).prop('disabled', true);
            $(t).html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');

            $.ajax({
                url: config.admin_url+'/users/status/'+user_id,
                success: function(data){  
                     $(t).prop('disabled', false);
                     if ($(t).hasClass( "btn-info" )) {
                        $(t).addClass('btn-danger').removeClass('btn-info');
                        $(t).html(lang.not_active);

                    }else{
                        $(t).addClass('btn-info').removeClass('btn-danger');
                        $(t).html(lang.active);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                   My.ajax_error_message(xhr);
               },
            });

        },
        delete: function(t) {
            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/users/' + id + '',
                data: { _method: 'DELETE', _token: $('input[name="_token"]').val() },
                success: function(data) {
                    Users_grid.api().ajax.reload();
                }
            });
        },
        empty: function() {
            $('#id').val(0);
            $('#active').find('option').eq(0).prop('selected', true);
            $('#gender').find('option').eq(0).prop('selected', true);
            $('#country').find('option').eq(0).prop('selected', true);
            $('#city').html("");
            $('#city').html('<option selected value="">' + lang.choose + '</option>');
            $('.image_box').html('<img src="' + config.url + '/no-image.png" class="image" width="150" height="80" />');
            $('#image').val('');
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            My.emptyForm();
        },
    };
}();
$(document).ready(function() {
    Users.init();
});