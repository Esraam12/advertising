var Locations_grid;
var parent_id = 0;

var Locations = function() {

    var init = function() {
        $.extend(lang, new_lang);
        $.extend(config, new_config);
        parent_id = config.parent_id;
        My.readImageMulti('image');
        handleRecords();
        handleSubmit();
    };


    var handleRecords = function() {
        Locations_grid = $('.dataTable').dataTable({
            //"processing": true,
            "serverSide": true,
            "ajax": {
                "url": config.admin_url + "/locations/data",
                "type": "POST",
                data: { parent_id: parent_id, _token: $('input[name="_token"]').val() },
            },
            "columns": [
                {
                    "data": "title",
                    "name": "location_translations.title"
                }, 
                
                {
                    "data": "active",
                    "name": "locations.active"
                }, 
                {
                    "data": "this_order",
                    "name": "locations.this_order"
                }, 
                {
                    "data": "num_of_users",
                    orderable: false
                }, 
                {
                    "data": "options",
                    orderable: false,
                    searchable: false
                }
            ],
             "order": [
                 [3, "asc"]
             ],
            "columnDefs": [{
                "className": "dt-center",
                "targets": 0
            }],
            "oLanguage": { "sUrl": config.url + '/datatable-lang-' + config.lang_code + '.json' }

        });
    }


    var handleSubmit = function() {
        $('#addEditLocationsForm').validate({
            rules: {
                active: {
                    required: true,
                },
                this_order: {
                    required: true,
                },
               
            },
            //messages: lang.messages,
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                $(element).closest('.form-group').find('.help-block').html('').css('opacity', 0);

            },
            errorPlacement: function(error, element) {
                $(element).closest('.form-group').find('.help-block').html($(error).html()).css('opacity', 1);
            }
        });


        var langs = JSON.parse(config.languages);
        for (var x = 0; x < langs.length; x++) {
            var ele = "input[name='title[" + langs[x] + "]']";
            $(ele).rules('add', {
                required: true
            });
        }
        


        $('#addEditLocationsForm .submit-form').click(function() {

            if ($('#addEditLocationsForm').validate().form()) {
                $('#addEditLocationsForm .submit-form').prop('disabled', true);
                $('#addEditLocationsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                setTimeout(function() {
                    $('#addEditLocationsForm').submit();
                }, 1000);
            }
            return false;
        });
        $('#addEditLocationsForm input').keypress(function(e) {
            if (e.which == 13) {
                if ($('#addEditLocationsForm').validate().form()) {
                    $('#addEditLocationsForm .submit-form').prop('disabled', true);
                    $('#addEditLocationsForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                    setTimeout(function() {
                        $('#addEditLocationsForm').submit();
                    }, 1000);
                }
                return false;
            }
        });



        $('#addEditLocationsForm').submit(function() {
            var id = $('#id').val();
            var action = config.admin_url + '/locations';
            var formData = new FormData($(this)[0]);
            formData.append('parent_id', parent_id);
            if (id != 0) {
                formData.append('_method', 'PATCH');
                action = config.admin_url + '/locations/' + id;
            }
            $.ajax({
                url: action,
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    $('#addEditLocationsForm .submit-form').prop('disabled', false);
                    $('#addEditLocationsForm .submit-form').html(lang.save);

                    if (data.type == 'success') {
                        My.toast(data.message);
                        if (id == 0) {
                            Locations.empty();
                        }


                    } else {
                        if (typeof data.errors !== 'undefined') {
                            for (i in data.errors) {
                                var message = data.errors[i];
                                if (i.startsWith('title')) {
                                    var key_arr = i.split('.');
                                    var key_text = key_arr[0] + '[' + key_arr[1] + ']';
                                    i = key_text;
                                }

                                $('[name="' + i + '"]')
                                    .closest('.form-group').addClass('has-error');
                                $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                            }
                        }
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    $('#addEditLocationsForm .submit-form').prop('disabled', false);
                    $('#addEditLocationsForm .submit-form').html(lang.save);
                    My.ajax_error_message(xhr);
                },
                dataType: "json",
                type: "POST"
            });


            return false;

        })




    }

    return {
        init: function() {
            init();
        },
        delete: function(t) {

            var id = $(t).attr("data-id");
            My.deleteForm({
                element: t,
                url: config.admin_url + '/locations/' + id,
                data: { _method: 'DELETE', _token: $('input[name="_token"]').val() },
                success: function(data) {
                    Locations_grid.api().ajax.reload();


                }
            });

        },
        error_message: function(message) {
            $.alert({
                title: lang.error,
                content: message,
                type: 'red',
                typeAnimated: true,
                buttons: {
                    tryAgain: {
                        text: lang.try_again,
                        btnClass: 'btn-red',
                        action: function() {}
                    }
                }
            });
        },
        empty: function() {
            $('#id').val(0);
            $('#active').find('option').eq(0).prop('selected', true);
            $('.has-error').removeClass('has-error');
            $('.has-success').removeClass('has-success');
            $('.help-block').html('');
            My.emptyForm();
        }
    };

}();
jQuery(document).ready(function() {
    Locations.init();
});