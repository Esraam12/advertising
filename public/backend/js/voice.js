const worker = new Worker(config.url + '/public/backend/js/worker.js');

class VoiceRecorder {
	constructor() {
		if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
			console.log("getUserMedia supported")
		} else {
			console.log("getUserMedia is not supported on your browser!")
		}
		this.mediaRecorder
		this.stream
		this.chunks = []
		this.isRecording = false

		this.recorderRef = document.querySelector("#recorder")
		this.playerRef = document.querySelector("#player")
		this.startRef = document.querySelector("#start")
		this.stopRef = document.querySelector("#stop")
		
		this.startRef.onclick = this.startRecording.bind(this)
		this.stopRef.onclick = this.stopRecording.bind(this)

		this.constraints = {
			audio: true,
			video: false
		}
		
	}

	handleSuccess(stream) {
		this.stream = stream
		this.stream.oninactive = () => {
			console.log("Stream ended!")
		};
		this.recorderRef.srcObject = this.stream
		//this.mediaRecorder = new MediaRecorder(this.stream)
		this.mediaRecorder = new window.mp3MediaRecorder.Mp3MediaRecorder(this.stream, {
			worker
		});
		console.log(this.mediaRecorder)
		this.mediaRecorder.ondataavailable = this.onMediaRecorderDataAvailable.bind(this)
		this.mediaRecorder.onstop = this.onMediaRecorderStop.bind(this)
		this.recorderRef.play()
		this.mediaRecorder.start()
	}

	handleError(error) {
		console.log("navigator.getUserMedia error: ", error)
	}
	
	onMediaRecorderDataAvailable(e) { this.chunks.push(e.data) }
	
	onMediaRecorderStop(e) { 
		const blob = new Blob(this.chunks, { 'type': 'audio/mpeg' })
			const audioURL = window.URL.createObjectURL(blob)



			
			var blobUrl = window.URL.createObjectURL(blob);

			var xhr = new XMLHttpRequest;
			xhr.responseType = 'blob';
			xhr.onload = function () {
				var recoveredBlob = xhr.response;
			
				var reader = new FileReader;
				reader.onload = function () {
					var audioData = document.querySelector("#audio-data");
					var blobAsDataUrl = reader.result;
					audioData.value = blobAsDataUrl;
				};
				reader.readAsDataURL(recoveredBlob);
			};

			xhr.open('GET', blobUrl);
			xhr.send();
			
			


			this.playerRef.src = audioURL
			this.chunks = []
			this.stream.getAudioTracks().forEach(track => track.stop())
            this.stream = null;
	}

	startRecording() {
		if (this.isRecording) return
		this.isRecording = true
		this.startRef.innerHTML = new_lang.recording + '....'
		this.playerRef.src = ''
		navigator.mediaDevices
			.getUserMedia(this.constraints)
			.then(this.handleSuccess.bind(this))
			.catch(this.handleError.bind(this))
	}
	
	stopRecording() {
		if (!this.isRecording) return
		this.isRecording = false
		this.startRef.innerHTML = '<i class="fa fa-microphone"></i> ' + new_lang.record
		this.recorderRef.pause()
		this.mediaRecorder.stop()
	}
	
}

window.voiceRecorder = new VoiceRecorder()

$('#stop').click(function(){
    $('#player').fadeIn()
})