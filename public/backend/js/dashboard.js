var Dashboard = function () {

    var init = function () {
        handleReports();
        
        $('#from').datepicker({
            format:'yyyy-mm-dd',
        }).on('changeDate', function (date) {
            populateEndDate();
        });
        $('#to').datepicker({
           format: 'yyyy-mm-dd'
        });
        if ($('#from').val()) {
            populateEndDate();
        }
       
    };

    var populateEndDate =  function () {
        var date = $('#from').datepicker('getDate');
        date.setDate(date.getDate());
        $('#to').datepicker('setStartDate', date);
    }

    var handleReports = function () {
        $('.submit-form').on('click', function () {
            var data = $("#dashboard-filter").serializeArray();
            var url = config.admin_url + "/";
            var params = {};
            $.each(data, function (i, field) {
                var name = field.name;
                var value = field.value;
                if (value) {
                    params[field.name] = field.value
                }

            });
            var query = $.param(params);
            if (!jQuery.isEmptyObject(params)) {
                url += '?' + query;
            }

            window.location.href = url;
            return false;
        })
    }
    return {
        init: function () {
            init();
        },
    };

}();
jQuery(document).ready(function () {
    Dashboard.init();
});
