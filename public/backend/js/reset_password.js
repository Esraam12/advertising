var errors = [];
var ResetPassword = function () {
        var init = function () {
            handle_reset_email();
        }
        var handle_reset_email = function () {
            $("#resetPasswordForm").validate({
                rules: {
                    email: {
                        required: true,
                        email:true
                    },
                    password: {
                        required: true,
                        minlength:8
                    },
                    password_confirmation: {
                        required: true,
                        equalTo:"#password"
                    },

                },
                messages: {
                    email: {
                        required: lang.required,
                        email: lang.email_not_valid
                    },
                    password: {
                        required: lang.required,
                    },
                    password_confirmation: {
                        required: lang.required,
                    },

                },
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    $(element).closest('.form-group').find('.help-block').html('');

                },
                errorPlacement: function (error, element) {
                    $(element).closest('.form-group').find('.help-block').html($(error).html());
                }

            });
            $('.submit-form').click(function () {
                if ($('#resetPasswordForm').validate().form()) {
                    $('#resetPasswordForm .submit-form').prop('disabled', true);
                    $('#resetPasswordForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                     setTimeout(function () {
                       $('#resetPasswordForm').submit();
                    }, 1000);
                }
                return false;
            });

            $('#resetPasswordForm input').keypress(function (e) {
                if (e.which == 13) {
                    if ($('#resetPasswordForm').validate().form()) {
                        $('#resetPasswordForm .submit-form').prop('disabled', true);
                        $('##resetPasswordForm .submit-form').html('<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>');
                        setTimeout(function () {
                           $('#resetPasswordForm').submit();
                        }, 1000);
                    }
                    return false;
                }
            });
          
            $('#resetPasswordForm').submit(function () {
                var action = config.admin_url + '/password/reset';
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: action,
                    data: formData,
                    dataType: "json",
                    type: "POST",
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data)
                    {
                        $('#resetPasswordForm .submit-form').prop('disabled', false);
                        $('#resetPasswordForm .submit-form').html(lang.reset_password);
                        
                        if (data.type == 'success') {
                            
                            window.location.href = config.admin_url + '/login';
                        } else {
                            if (typeof data.errors !== 'undefined') {
                                for (i in data.errors)
                                {
                                    var message = data.errors[i];
                                   $('[name="' + i + '"]').closest('.form-group').addClass('has-error');
                                   $('[name="' + i + '"]').closest('.form-group').find(".help-block").html(message).css('opacity', 1);
                                }
                            }
                            if (typeof data.message !== 'undefined') {
                                $('#alert-message').html(data.message).fadeIn(400).delay(3000).fadeOut(400);
                            }
                        }


                    },
                    error: function (xhr, textStatus, errorThrown) {
                        $('#resetPasswordForm .submit-form').prop('disabled', false);
                        $('#resetPasswordForm .submit-form').html(lang.send_reset_link);
                        My.ajax_error_message(xhr);
   
                    },
                });

                return false;
            });

        }

        return {
            init: function () {
                init();
            }
        }

    }();

    jQuery(document).ready(function () {
        ResetPassword.init();
    });


