<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */


$languages = Config::get('app.locales');
$defaultLanguage = 'en';
if ($defaultLanguage) {
    $defaultLanguageCode = $defaultLanguage;
} 

$currentLanguageCode = Request::segment(1, $defaultLanguageCode);
if (in_array($currentLanguageCode, $languages)) {
    $defaultLanguageCode = $currentLanguageCode;
}

Route::group(['namespace' => 'Front', 'middleware' => 'front.lang'], function () use($defaultLanguageCode){
        App::setLocale($defaultLanguageCode);
        Route::get('getLocations/{country}', 'AjaxController@getLocations');
       
    

        Route::get('/', 'HomeController@index')->name('home');
    
        Auth::routes();



         /** profile */
        Route::get('profile', 'ProfileController@show')->name('front.user_profile');
        Route::get('profile/edit', 'ProfileController@edit')->name('front.edit_profile');
        Route::post('profile/edit', 'ProfileController@update')->name('front.update_profile');

       
});



Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/error', 'AdminController@error')->name('admin.error');
    Route::get('/change_lang', 'AjaxController@change_lang')->name('ajax.change_lang');
    Route::post('/change_sidebar_state', 'AjaxController@change_sidebar_state');
    Route::get('getCategories/{category_id}', 'AjaxController@getCategories');
    Route::get('getSpecialists/{category_id}', 'AjaxController@getSpecialists');
    Route::post('delete_image', 'AjaxController@deleteImage');
    Route::get('getLocations/{country}', 'AjaxController@getLocations');

    Route::get('statistics', 'AdminController@getStatistics')->name('statistics');

    Route::get('profile', 'ProfileController@index');
    Route::patch('profile', 'ProfileController@update');

    Route::resource('groups', 'GroupsController');
    Route::post('groups/data', 'GroupsController@data');

    Route::resource('admins', 'AdminsController');
    Route::post('admins/data', 'AdminsController@data');

    Route::resource('locations', 'LocationsController');
    Route::post('locations/data', 'LocationsController@data');  
    Route::resource('banners', 'BannersController');
    Route::post('banners/data', 'BannersController@data');  
    Route::resource('users', 'UsersController');
    Route::post('users/data', 'UsersController@data');
    Route::get('settings', 'SettingsController@index')->name('settings');
    Route::post('settings', 'SettingsController@store');

    
    Route::group(['namespace' => 'Auth'], function() {

        Route::get('login', 'LoginController@showLoginForm')->name('admin.login');
        Route::post('login', 'LoginController@login')->name('admin.login.submit');
        Route::get('logout', 'LoginController@logout')->name('admin.logout');
        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('admin.password.reset');
        Route::post('password/reset', 'ResetPasswordController@reset')->name('admin.password.update');
    });
  
});


