<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

 Route::group(['namespace' => 'Api'], function () {

    Route::get('token', 'BasicController@getToken');
    Route::get('settings', 'BasicController@getSettings');
    Route::get('locations', 'BasicController@getLocations');
    Route::get('categories', 'BasicController@getCategories');
    Route::get('packages', 'BasicController@getPackages');
    Route::get('currencies', 'BasicController@getCurrencies');
    Route::get('slider', 'BasicController@getSlider');
    Route::post('send_contact_message', 'BasicController@sendContactMessage');
    Route::post('send_consultation', 'BasicController@sendConsultation');    


    Route::post('login', 'LoginController@login');
    Route::post('register', 'RegisterController@register');
   

    Route::post('password/verify', 'PasswordController@verify');
    Route::post('password/reset', 'PasswordController@reset');

    Route::resource('articles', 'ArticlesController');
    Route::resource('sessions', 'SessionsController');
    Route::resource('specialists', 'SpecialistsController');
    Route::get('posts', 'PostsController@index');
    
    Route::get('test', 'BasicController@test');
    

    Route::group(['middleware' => 'jwt.auth'], function () {
        
        /** user */
        Route::post('user/update', 'UserController@update');
        Route::get('user_subscription', 'UserController@userSubscription');
        Route::post('renew_subscription', 'UserController@renewSubscription');

        /** favourites */
        Route::post('favourites', 'BasicController@handleFavourites');


        /** posts */
        Route::post('posts','PostsController@store');
        Route::get('posts/{id}', 'PostsController@show');
        Route::delete('posts/{id}', 'PostsController@destroy');
      
        Route::post('rate', 'BasicController@rate');
        Route::get('rates', 'BasicController@getRates');
        

        Route::get('logout', 'UserController@logout');
        Route::get('update_lang', 'UserController@updateLang');
        Route::get('get_user', 'UserController@getUser');

        Route::get('notifications', 'BasicController@GetUserNotifications');
        Route::post('report', 'BasicController@abuseReport');
        
       
        
    });
});
