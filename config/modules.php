<?php

return [

    
    'admin_modules' => [
        [
            'name' =>    'admins_section',
            'actions' => 'open'
        ],
        [
            'name' =>    'groups',
            'actions' => 'open,add,edit,delete'
        ],
        [
            'name' =>    'admins',
            'actions' => 'open,add,edit,delete'
        ],
        [
            'name' =>    'settings',
            'actions' => 'open'
        ],
        [
            'name' =>    'users',
            'actions' => 'open,add,edit,delete'
        ],
        [
            'name' =>    'locations',
            'actions' => 'open,add,edit,delete'
        ],
        [
            'name' =>    'banners',
            'actions' => 'open,add,edit,delete'
        ],
        
    ]
    

];
