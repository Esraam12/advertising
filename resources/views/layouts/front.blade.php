<!doctype html>
<html>
    <head>
        @include('components/front/meta')
    </head>

    <body class="d-flex flex-column">
        @include('components/front/header')
        <main>
            @yield('content')
        </main>
        @include('components/front/footer')

        @include('components/front/scripts')
    </body>
</html>
