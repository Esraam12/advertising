<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Advertising Dashbaord|Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{url('public/backend/plugins')}}/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('public/backend/plugins')}}/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('public/backend/plugins')}}/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('public/backend/plugins')}}/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{url('public/backend/plugins')}}/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('public/backend/plugins')}}/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{url('public/backend/css')}}/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{url('public/backend/css')}}/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="{{url('public/backend/css')}}/login-4.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <script>
            var config = {
                admin_url: " {{ url('admin') }}",
                asset_url: " {{ url('public//') }}",
            };
            var lang = {
                required: "{{ _lang('app.this_field_is_required')}}",
                email_not_valid: "{{ _lang('app.email_is_not_valid')}}",
                send_reset_link: "{{ _lang('app.send_password_reset_link') }}",
                close: "{{ _lang('app.close') }}",
                reset_password: "{{ _lang('app.reset_password') }}"
            };
        </script>
        <style type="text/css">
            .login .content{
                background-color: #fff;
                min-height: 250px;
                min-width: 400px;
            }
            .login .content h3{
                font-weight: 400 !important;
                text-align: center;
                font-size: 28px;
            }
            .login .content .form-control {
                height: 43px;
            }
            .copyright a{
                color:#dfdfdf;
            }
        </style>
    </head>
    <!-- END HEAD -->

    <body class="login" style="background-color:#364150!important">
        <!-- BEGIN LOGO -->
        <div class="logo">
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        
            <!-- BEGIN LOGIN FORM -->
            @yield('content')


        
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright">
                    All Rights Reserved © Esraa Mostafa   Co. 2020
        </div>
    
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{url('public/backend/plugins')}}/jquery.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/bootbox/bootbox.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{url('public/backend/plugins')}}/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{url('public/backend/scripts')}}/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->

        <script src="{{url('public/backend/scripts')}}/app.js" type="text/javascript"></script>
        <script src="{{url('public/backend/scripts')}}/login-4.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/plugins')}}/tinymce/tinymce.min.js" type="text/javascript"></script>
        <script src="{{url('public/backend/js')}}/my.js" type="text/javascript"></script>
        @yield('scripts')
        
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>