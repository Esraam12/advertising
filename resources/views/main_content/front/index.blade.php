@extends('layouts.front')
@section('pageTitle',_lang('app.advertising'))
@section('js')
    <script>
        $('.toast').toast('show');
    </script>
@endsection

@section('content')
        <!-- banner -->
        @if(isset($banners) && !empty($banners))
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                  @foreach ($banners as $banner)
                  <div class="carousel-item {{ $banner->this_order == 1? 'active' : ''}}" data-interval="{{$banner->duration}}" style="height:370px;background-color:#343434; background-repeat: no-repeat;background-size: cover;">
                      <div class="row mt-3 mb-2">
                          <div class="col-md-4">
                    <img style="width:600px;"src="{{url('public/uploads/banners/')}}/{{$banner->image}}" alt="{{$banner->title}}">
                </div>
                <div class="col-md-8">
             <div class="carousel-caption d-none d-md-block" style="margin-bottom: 20% !important;color:white">
                 <h2 class="mb-3">{{$banner->title}}</h2>
                <p>{{$banner->description}}</p>
                  </div>
                </div>
                </div>
                </div>
                  @endforeach
              
          </div>
        @else
        <section class="banner">
        </section>
        @endif
        
        <!-- # end banner # -->

    
@endsection