@extends('layouts.front')
@section('pageTitle',_lang('app.profile'))
@section('content')
 <!-- banner -->
 <section class="banner inner-header inner-details-header">
            <h1>{{_lang('app.profile')}}</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{url($lang_code)}}">{{_lang('app.home')}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{_lang('app.profile')}}</li>
                </ol>
            </nav>
        </section>
        <!-- # end banner # -->

        <!-- about -->
        <section class="aricles inner-articles">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-5">
                        @include('components/front/profile/sidebar_profile')
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="profile-cont animated wow fadeInUp">
                            <h4 class="cont-title">{{_lang('app.profile')}}</h4>
                            <div class="row">
                                <div class="col-12">
                                    <ul class="profile-list">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <li>
                                                    <label>{{_lang('app.name')}}</label>
                                                    <span>{{$user->username}}</span>
                                                </li>
                                            </div>
                                            <div class="col-md-6">
                                                <li>
                                                    <label>{{_lang('app.email')}}</label>
                                                    <span>{{$user->email}}</span>
                                                </li>
                                            </div>
                                            <div class="col-md-6">
                                                <li>
                                                    <label>{{_lang('app.country')}}</label>
                                                    <span>{{$user->country}}</span>
                                                </li>
                                            </div>
                                            <div class="col-md-6">
                                                <li>
                                                    <label>{{_lang('app.city')}}</label>
                                                    <span>{{$user->city}}</span>
                                                </li>
                                            </div>                                   
                                            <div class="col-md-6">
                                                <li>
                                                    <label>{{_lang('app.age')}}</label>
                                                    <span>{{$user->age}}</span>
                                                </li>
                                            </div>
                                            <div class="col-md-6">
                                                <li>
                                                    <label>{{_lang('app.gender')}}</label>
                                                    <span>{{$user->gender==1 ? 'male' :'female'}}</span>
                                                </li>
                                            </div>  
                                        </div>                                                                  
                                    </ul>
                                    <div class="row">
                                        <div class="col-6 text-center">
                                            <a href="{{route('front.edit_profile')}}"><button class="pink-btn"><i class="fas fa-pen"></i> {{_lang('app.edit_profile')}}</button></a>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>                           
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection