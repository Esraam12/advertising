@extends('layouts.front')
@section('pageTitle',_lang('app.edit_profile'))
@section('js')
    <script type="text/javascript" src="{{ url('public/front/scripts/profile.js') }}"></script>
@endsection
@section('content')
   <!-- banner -->
   <section class="banner inner-header inner-details-header">
            <h1>{{_lang('app.profile')}}</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{url($lang_code)}}">{{_lang('app.home')}}</a></li>
                    <li class="breadcrumb-item"><a href="{{route('front.user_profile')}}">{{_lang('app.profile')}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{_lang('app.edit_profile')}}</li>
                </ol>
            </nav>
    </section>
        <!-- # end banner # -->

        <!-- about -->
        <section class="aricles inner-articles">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-5">
                    @include('components/front/profile/sidebar_profile')
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="profile-cont animated wow fadeInUp">

                         <form id="editProfileForm" enctype="multipart/form-data">
                            <h4 class="cont-title">{{_lang('app.edit_profile')}}</h4>
                            <div class="form register-form animated fadeInUp"> 
                                <div class="row mb-0">
                            <div class="form-group form-md-line-input col-md-4 mb-0">
                                    <div class="image_box">
                                        <img src="{{url('public/uploads/users').'/'.$user->image}}" width="135" height="120" class="image" />
                                    </div>
                                    <input type="file" name="image" id="image" style="visibility: hidden">
                                    <span class="invalid-feedback">
                                    </span>
                               </div>  
                               </div>                            
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{_lang('app.name')}}</label>
                                            <input type="text" placeholder="{{_lang('app.name')}}" class="form-control" name="name" value="{{$user->username}}">
                                            <span class="invalid-feedback">
                                          </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{_lang('app.email')}}</label>
                                            <input type="email" placeholder="{{_lang('app.email')}}" class="form-control" name="email" value="{{$user->email}}">
                                            <span class="invalid-feedback">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{_lang('app.age')}}</label>
                                            <input type="text" placeholder="{{_lang('app.age')}}" class="form-control"name="age" value="{{$user->age}}">
                                            <span class="invalid-feedback">
                                           </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{_lang('app.gender')}}</label>
                                            <select class="form-control" id="gender" name="gender">
                                                <option value="">{{_lang('app.choose')}}</option>
                                                <option value="1" {{$user->gender == 1 ? 'selected' : ''}}>{{_lang('app.male')}}</option>
                                                <option value="2" {{$user->gender == 2 ? 'selected' : ''}}>{{_lang('app.female')}}</option>
                                            </select> 
                                            <span class="invalid-feedback">
                                          </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12"></div>
                                       <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>{{_lang('app.country')}}</label>
                                                <select class="form-control" id="country" name="country">
                                                    <option value="">{{_lang('app.choose')}}</option>
                                                    @foreach ($locations as $location)
                                                    <option value="{{$location->id}}" {{$user->country_id == $location->id ? 'selected' : ''}}>
                                                        {{$location->title}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="invalid-feedback">
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>{{_lang('app.city')}}</label>
                                                <select class="form-control" id="city" name="city">
                                                    <option value="">{{_lang('app.choose')}}</option>
                                                    @foreach ($cities as $city)
                                                    <option value="{{$city->id}}" {{$city->id == $user->location_id ? 'selected' : ''}}>{{$city->title}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                                <span class="invalid-feedback">
                                                </span>
                                            </div>
                                        </div>
                                    
                                    
                                </div>                                   
                                <button class="pink-btn block-btn-center submit-form">{{_lang('app.save')}}</button>
                            </div>
                        </div>
                    </form>

                    </div>
                </div>
            </div>
        </section>
@endsection