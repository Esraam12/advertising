@extends('layouts.front')
@section('pageTitle',_lang('app.error'))
@section('js')
<script type="text/javascript" src="{{ url('public/front/scripts/err404.js') }}"></script>
@endsection
@section('content')
<!-- banner -->
<section class="banner inner-header inner-list-header">
  <h1 class="animated fadeInUp"> {{_lang('app.error')}}</h1>
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb animated fadeInUp">
      <li class="breadcrumb-item"><a href="{{route('home')}}">{{_lang('app.home')}}</a></li>
      <li class="breadcrumb-item active" aria-current="page"> {{_lang('app.error')}}</li>
    </ol>
  </nav>
</section>
<!-- # end banner # -->

<!-- about -->
<section class="">
  <div class="container">
    <div class="row justify-content-center">
      <!-- Error Page -->
      <div class="error col-xs-12 justify-content-center" style="float:left !important; direction:ltr;">
        <div class="container-floud">
          <div class="col-xs-12 ground-color text-center">
            <div class="container-error-404">
              <div class="clip">
                <div class="shadow"><span class="digit thirdDigit"></span></div>
              </div>
              <div class="clip">
                <div class="shadow"><span class="digit secondDigit"></span></div>
              </div>
              <div class="clip">
                <div class="shadow"><span class="digit firstDigit"></span></div>
              </div>
              <div class="msg">{{_lang('app.OH!')}}<span class="triangle"></span></div>
            </div>
            <h2 class="h1">{{_lang('app.Sorry!_Page_not_found')}}</h2>
          </div>
        </div>
      </div>
      <!-- Error Page -->
    </div>
    <div class=" d-flex justify-content-center">
      <a href="#" class="pink-btn" onClick="window.history.back();"> <i class="fas fa-arrow-left"></i>
        {{ _lang('app.back_to_the_website') }}</a>
    </div>
    <br>
  </div>

</section>
<!-- # end about # -->


@endsection