@extends('layouts.front')
@section('pageTitle',_lang('app.login_to_our_website'))
@section('js')
<script type="text/javascript" src="{{ url('public/front/scripts/login.js') }}"></script>
@endsection
@section('content')
    <section class="banner inner-header inner-account-header">
        <h1 class="animated fadeInUp">{{_lang('app.login')}}</h1>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb animated fadeInUp">
                <li class="breadcrumb-item"><a href="{{url($lang_code)}}">{{_lang('app.home')}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{_lang('app.login')}}</li>
            </ol>
        </nav>
    </section>
    <!-- # end banner # -->
    <section class="login">
        <div class="form animated fadeInUp" style="min-width:500px">
            <h3>{{_lang('app.welcome_back')}}</h3>
            <p class="main"><a href="{{route('register')}}">{{_lang('app.you_do_not_have_an_account?')}}</a>
            </p>

           
            <div class="alert alert-danger alert-dismissible fade show" id="alertMessage" role="alert" style="{{session()->has('message') ? 'display:block;':'display:none;'}}">
                <strong id="message">{{session()->has('message') ? session()->get('message'):''}}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="POST" action="{{ route('login') }}" id="loginForm">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>{{_lang('app.username')}}</label>
                    <div class="input-group">   
                        <input type="text" placeholder="{{_lang('app.username')}}" class="form-control {{ isset($errors)  ? 'is-invalid' : ''}}" name="username" id="username"
                            value="">
                        <span class="invalid-feedback">
                            {{ isset($errors)  ?  $errors->first('username') : ''}}
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label>{{_lang('app.password')}}</label>
                    <div class="input-group">   
                        <input type="password" placeholder="{{_lang('app.password')}}" class="form-control {{ isset($errors) && $errors->has('password') ? 'is-invalid' : ''}}" name="password" id="password"
                            value="">
                        <span class="invalid-feedback">
                            {{ isset($errors) && $errors->has('password') ?  $errors->first('password') : ''}}
                        </span>
                    </div>
                </div>
            
            <button class="pink-btn btn-block mb-3 submit-form" type="submit">{{_lang('app.login')}}</button>
        </form>
        </div>
    </section>
@endsection
