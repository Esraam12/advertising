@extends('layouts.front')
@section('pageTitle',_lang('app.register'))
@section('js')
    <script type="text/javascript" src="{{ url('public/front/scripts/register.js') }}"></script>
@endsection
@section('content')
<section class="banner inner-header inner-account-header">
    <h1 class="animated fadeInUp">{{_lang('app.register')}}</h1>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb animated fadeInUp">
            <li class="breadcrumb-item"><a href="{{url($lang_code)}}">{{_lang('app.home')}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{_lang('app.register')}}</li>
        </ol>
    </nav>
</section>
<!-- # end banner # -->
<section class="login">
    <div class="container">
        <div class="form register-form animated fadeInUp">
            <h3>{{_lang('app.welcome')}}</h3>
            <p class="main"><a href="{{route('login')}}">{{_lang('app.already_a_member_?_sign_in')}}</a></p>
            <form method="POST" action="{{ route('register') }}" id="registerForm">
                {{ csrf_field() }}
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                    <label>{{_lang('app.name')}}</label>
                        <input type="text" placeholder="{{_lang('app.name')}}" class="form-control" name="name" id="name" value="{{ old('name') }}">
                        <span class="invalid-feedback">
                        </span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>{{_lang('app.email')}}</label>
                        <input type="email" placeholder="{{_lang('app.email')}}" class="form-control" name="email" id="email" value="{{ old('email') }}">
                        <span class="invalid-feedback">
                        </span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>{{_lang('app.age')}}</label>
                        <input type="number" placeholder="{{_lang('app.age')}}" class="form-control" name="age" id="age" value="{{ old('age') }}">
                        <span class="invalid-feedback">
                            
                        </span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>{{_lang('app.gender')}}</label>
                        <select class="form-control" name="gender" id="gender">
                        <option value="">{{_lang('app.choose')}}</option>
                        <option value="1">{{_lang('app.male')}}</option>
                        <option value="2">{{_lang('app.female')}}</option>
                        </select>
                        <span class="invalid-feedback">
                            
                        </span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group ">
                        <label>{{_lang('app.password')}}</label>
                        <div class="input-group">
                        <input type="password" placeholder="{{_lang('app.password')}}" class="form-control" name="password" id="password" value="">
                            <span class="invalid-feedback">
                                
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>{{_lang('app.country')}}</label>
                        <select class="form-control" name="country" id="country">
                            <option value="">{{_lang('app.choose')}}</option>
                            @foreach ($locations as $location)
                                <option value="{{$location->id}}">{{$location->title}}</option>
                            @endforeach
                        </select>
                        <span class="invalid-feedback">
                            
                        </span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>{{_lang('app.city')}}</label>
                        <select class="form-control" name="city" id="city">
                            <option value="">{{_lang('app.choose')}}</option>
                        </select>
                        <span class="invalid-feedback">
                        </span>
                    </div>
                </div>
            </div>
                <button class="pink-btn btn-block mb-3 submit-form" type="submit">{{_lang('app.register')}}</button>
            </form>
        </div>
    </div>
</section>


@endsection
