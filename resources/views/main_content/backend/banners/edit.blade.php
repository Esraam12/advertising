@extends('layouts.backend')

@section('pageTitle',_lang('app.edit_banner'))

@section('breadcrumb')
<li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

<li><a href="{{route('banners.index')}}">{{_lang('app.banners')}}</a> <i class="fa fa-circle"></i></li>

<li><span> {{_lang('app.edit')}}</span></li>

<li><span> {{_lang('app.banner')}}</span></li>

@endsection

@section('js')
<script src="{{url('public/backend/js')}}/banners.js" type="text/javascript"></script>
@endsection
@section('content')
<form role="form" id="addEditBannerForm" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.banner_title') }}</h3>
        </div>
        <div class="panel-body">


            <div class="form-body">
                <input type="hidden" name="id" id="id" value="{{ $banner->id }}">

                <div class="form-body">
                    @foreach ($languages as $key => $value)

                    <div class="form-group form-md-line-input col-md-6">
                        <input type="text" class="form-control" id="title[{{ $key }}]" name="title[{{ $key }}]"
                            value="{{ $translations["$key"]->title }}">
                        <label for="title">{{_lang('app.title') }} {{ _lang('app.'.$value.'') }}</label>
                        <span class="help-block"></span>
                    </div>

                    @endforeach

                </div>


            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.banner_description') }}</h3>
        </div>
        <div class="panel-body">
    
    
            <div class="form-body">
                <input type="hidden" name="id" id="id" value="0">
    
                <div class="form-body">
                    @foreach ($languages as $key => $value)
    
                    <div class="form-group form-md-line-input col-md-6">
                        <textarea  class="form-control" id="description[{{ $key }}]" name="description[{{ $key }}]"
                            rows="7">{{ $translations["$key"]->description }}</textarea>
                        <label for="description">{{_lang('app.description') }} {{ _lang('app.'.$value.'') }}</label>
                        <span class="help-block"></span>
                    </div>
    
                    @endforeach
    
                </div>
    
    
            </div>
        </div>
    </div>





    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.banner_photo') }}</h3>
        </div>
        <div class="panel-body">

            <div class="form-body">
            <h4 class="text-info">Hints:</h4>
           <h5>Minimum image size : 1130x474.15</h5>

                <div class="form-group form-md-line-input col-md-2">
                    <div class="image_box">
                       <img src="{{url('public/uploads/banners').'/'.$banner->image}}" width="100" height="80" class="image" />
                    </div>
                    <input type="file" name="image" id="image" style="visibility: hidden">
                    <span class="help-block"></span>
                </div>


                <div class="clearfix"></div>
                <hr>


            </div>
        </div>

    </div>


    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">


            <div class="form-body">



                <div class="form-group form-md-line-input col-md-3">
                <input type="number" class="form-control" id="this_order" name="this_order" value="{{$banner->this_order}}">
                    <label for="this_order">{{_lang('app.this_order') }}</label>
                    <span class="help-block"></span>
                </div>

                <div class="form-group form-md-line-input col-md-3">
                    <select class="form-control edited" id="active" name="active">
                       <option value="1" {{ $banner->active == 1 ?'selected' : '' }}>{{ _lang('app.active') }}</option>
                        <option value="0" {{ $banner->active == 0 ?'selected' : '' }}>{{ _lang('app.not_active') }}</option>
                    </select>
                    <label for="status">{{_lang('app.status') }}</label>
                    <span class="help-block"></span>
                </div>
                <div class="form-group form-md-line-input col-md-3">
                    <input type="number" class="form-control" id="duration" name="duration" value="{{$banner->duration}}">
                    <label for="duration">{{_lang('app.duration') }}</label>
                    <span class="help-block"></span>
                </div>
                <div class="form-group form-md-line-input col-md-3">
                    <input type="number" class="form-control" id="frequency" name="frequency" value="{{$banner->frequency}}">
                    <label for="frequency">{{_lang('app.frequency_of_appearance') }}</label>
                    <span class="help-block"></span>
                </div>
                <div class="form-group form-md-line-input col-md-3 select">
                    <select class="form-control edited" id="country" name="country">
                        <option value="">{{_lang('app.choose')}}</option>
                        @foreach ($locations as $location)
                        <option value="{{$location->id}}" {{$city->parent_id == $location->id ? 'selected' : ''}}>{{$location->title}}</option>
                        @endforeach
                    </select>
                    <label for=" country">{{_lang('app.country') }}</label>
                    <span class="help-block"></span>
                </div>

                <div class="form-group form-md-line-input col-md-3 select">
                    <select class="form-control edited" id="city" name="city">
                        <option value="">{{_lang('app.choose')}}</option>
                        @foreach ($cities as $city)
                        <option value="{{$city->id}}" {{$city->id == $banner->location_id ? 'selected' : ''}}>{{$city->title}}</option>
                        @endforeach
                    </select>
                    <label for=" city">{{_lang('app.city') }}</label>
                    <span class="help-block"></span>
                </div>

            </div>
        </div>

        <div class="panel-footer text-center">
            <button type="button" class="btn btn-info submit-form">{{_lang('app.save') }}</button>
        </div>


    </div>


</form>
<script>
    var new_lang = {

};
var new_config = {
    
};

</script>
@endsection