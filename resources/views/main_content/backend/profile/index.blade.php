@extends('layouts.backend')

@section('pageTitle',_lang('app.profile'))


@section('js')
<script src="{{url('public/backend/js')}}/profile.js" type="text/javascript"></script>
@endsection
@section('content')
@if ($User->type == 2)
<div class="row">
    <div class="row">
        <div class="col-md-6">
            <div class="col-md-12">

                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{{ _lang('app.personal')}}
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-hover">

                                <tbody>
                                    <tr>
                                        <td>{{ _lang('app.name')}}</td>
                                        <td>{{$specialist->name}}</td>

                                    </tr>
                                    <tr>
                                        <td>{{ _lang('app.phone')}}</td>
                                        <td>{{$specialist->phone}}</td>

                                    </tr>
                                    <tr>
                                        <td>{{ _lang('app.email')}}</td>
                                        <td>{{$specialist->email}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ _lang('app.status')}}</td>
                                        <td>{{$specialist->active}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ _lang('app.description')}}</td>
                                        <td>{{$specialist->description}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ _lang('app.image')}}</td>
                                        <td><img style="width: 30;height: 30px;" alt="" src="{{$specialist->image}}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{ _lang('app.major_specialization')}}</td>
                                        <td>{{$specialist->category}}</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->


            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">

                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{{ _lang('app.sub_specialists')}}
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-hover">

                                <tbody>
                                    @foreach ($specialist->categories as $item)
                                    <tr>
                                        <td> <img style="width: 30px;height: 30px;" alt="" src="{{$item->image}}">
                                            {{$item->title}} </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->


            </div>
        </div>

    </div>
    <div class="row">


        <div class="col-md-6">
            <div class="col-md-12">

                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box yellow-crusta">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{{ _lang('app.rates')}}
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-hover">




                                <tbody>
                                    <tr>
                                        <td>
                                            
                                                {{ _lang('app.sessions_rate')}}
                                            
                                        </td>
                                        <td>
                                            @for ($i = 0; $i < $specialist->sessions_rate ; $i++)
                                                <span class="text-warning fa fa-star"></span>
                                                @endfor
                                                @for ($i = 0; $i < 5 - $specialist->sessions_rate; $i++)
                                                    <span class="fa fa-star"></span>
                                                    @endfor
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           
                                                {{ _lang('app.articles_rate')}}
                                           
                                        </td>
                                        <td>
                                            @for ($i = 0; $i < $specialist->artilces_rate ; $i++)
                                                <span class="text-warning fa fa-star"></span>
                                                @endfor
                                                @for ($i = 0; $i < 5 - $specialist->artilces_rate; $i++)
                                                    <span class="fa fa-star"></span>
                                                    @endfor

                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                           
                                                {{ _lang('app.questions_rate')}}
                                            
                                        </td>
                                        <td>


                                            @for ($i = 0; $i < $specialist->questions_rate ; $i++)
                                                <span class="text-warning fa fa-star"></span>
                                                @endfor
                                                @for ($i = 0; $i < 5 - $specialist->questions_rate; $i++)
                                                    <span class="fa fa-star"></span>
                                                    @endfor

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{ _lang('app.total_rate')}}</td>
                                        <td>
                                            @for ($i = 0; $i < $specialist->total_rate ; $i++)
                                                <span class="text-warning fa fa-star"></span>
                                                @endfor
                                                @for ($i = 0; $i < 5 - $specialist->total_rate; $i++)
                                                    <span class="fa fa-star"></span>
                                                    @endfor
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->


            </div>
        </div>
    </div>


</div>  

@endif
<form role="form"  id="addEditProfileForm"  enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.profile') }}</h3>
        </div>
        <div class="panel-body">


            <div class="form-body">
                @if ($User->type == 1)
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" id="username" name="username" value="{{$User->username}}">
                    <label for="username">{{_lang('app.username')}}</label>
                    <span class="help-block"></span>
                </div>
                 <div class="form-group form-md-line-input">
                    <input type="password" class="form-control" id="password" name="password">
                    <label for="password">{{_lang('app.password')}}</label>
                    <span class="help-block"></span>
                </div>
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" id="email" name="email" value="{{$User->email}}">
                    <label for="email">{{_lang('app.email')}}</label>
                    <span class="help-block"></span>
                </div>

                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" id="phone" name="phone" value="{{$User->phone}}">
                    <label for="phone">{{_lang('app.phone')}}</label>
                    <span class="help-block"></span>
                </div>
                @endif
                @if ($User->type == 2)
                <div class="form-group form-md-line-input">
                    <input type="password" class="form-control" id="old_password" name="old_password">
                    <label for="old_password">{{_lang('app.old_password')}}</label>
                    <span class="help-block"></span>
                </div>

                <div class="form-group form-md-line-input">
                    <input type="password" class="form-control" id="password" name="password">
                    <label for="password">{{_lang('app.password')}}</label>
                    <span class="help-block"></span>
                </div>

                <div class="form-group form-md-line-input">
                    <input type="password" class="form-control" id="confirm_password" name="confirm_password">
                    <label for="confirm_password">{{_lang('app.confirm_password')}}</label>
                    <span class="help-block"></span>
                </div>
                @endif
            </div>
            <!--Table Wrapper Finish-->
        </div>
        <div class="panel-footer text-center">
            <button type="button" class="btn btn-info submit-form"
                    >{{_lang('app.save') }}</button>
        </div>

    </div>
</form>
@endsection