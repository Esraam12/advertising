@extends('layouts.backend_auth')

@section('content')
<div class="content">
    <div id="alert-message" class="alert-danger text-center"></div>
    <div id="success-message" class="alert-success text-center"></div>
         @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
        @endif

            <br>
            <form class="login-form"  id="resetEmailForm"  method="post"  action="{{ route('admin.password.email') }}">
                {{ csrf_field() }}
                <h3 class="form-title font-green">{{ _lang('app.reset_password') }}</h3>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">{{_lang('app.email')}}</label>
                    <div class="input-icon">
                        <i class="fa fa-paper-plane"></i>
                        <input class="form-control form-control-solid placeholder-no-fix valid" type="email" autocomplete="off" placeholder="{{_lang('app.email')}}" name="email" id="email" />
                    </div>
                    <div class="help-block">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
               
                <div class="form-actions">
                   <button type="submit" class="btn green pull-right submit-form btn-block btn-lg"> {{ _lang('app.send_password_reset_link') }} </button>
                </div>
            </form>

            <!-- END LOGIN FORM -->
</div>
@endsection

@section('scripts')
    <script src="{{url('public/backend/js')}}/reset_emial.js" type="text/javascript"></script>
@endsection



