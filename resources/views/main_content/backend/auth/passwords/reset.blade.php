@extends('layouts.backend_auth')

@section('content')
<div class="content" style="min-height: 450px">
    <div id="alert-message" class="alert-danger text-center"></div>
    <div id="success-message" class="alert-success text-center"></div>
            <br>
            <form class="login-form"  id="resetPasswordForm"  method="post"  action="{{ route('admin.password.update') }}">
                {{ csrf_field() }}
                <h3 class="form-title font-green">{{ _lang('app.reset_password') }}</h3>
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">{{_lang('app.email')}}</label>
                    <div class="input-icon">
                        <i class="fa fa-paper-plane"></i>
                        <input class="form-control form-control-solid placeholder-no-fix valid" type="email" autocomplete="off" placeholder="{{_lang('app.email')}}" name="email" id="email" />
                    </div>
                    <div class="help-block">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>

                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">{{_lang('app.password')}}</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control form-control-solid placeholder-no-fix valid" type="password" autocomplete="off" placeholder="{{_lang('app.password')}}" name="password" id="password" />
                    </div>
                    <div class="help-block">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>


                <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">{{_lang('app.password_confirmation')}}</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control form-control-solid placeholder-no-fix valid" type="password" autocomplete="off" placeholder="{{_lang('app.confirm_password')}}" name="password_confirmation" id="password_confirmation" />
                    </div>
                    <div class="help-block">
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
               
                <div class="form-actions">
                   <button type="submit" class="btn green pull-right submit-form btn-block btn-lg"> {{ _lang('app.reset_password') }} </button>
                </div>
            </form>
</div>
            <!-- END LOGIN FORM -->
@endsection

@section('scripts')
    <script src="{{url('public/backend/js')}}/reset_password.js" type="text/javascript"></script>
@endsection



