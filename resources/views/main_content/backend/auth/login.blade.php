@extends('layouts.backend_auth')

@section('content')
<div class="content">
    <div id="alert-message" class="alert-danger text-center">

            </div>
            <br>
            <form class="login-form"  id="login-form"  method="post"  action="{{ route('admin.login.submit') }}">
                {{ csrf_field() }}
                <h3 class="form-title font-green">{{ _lang('app.login') }}</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span>{{ _lang('app.Enter_any_username_and_password') }}.</span>
                </div>

                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">{{_lang('app.username')}}</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control form-control-solid placeholder-no-fix valid" type="text" autocomplete="off" placeholder="{{_lang('app.username')}}" name="username" id="username" />
                    </div>
                    <div class="help-block"></div>

                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">{{_lang('app.password')}}</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="{{_lang('app.password')}}" name="password" id="password" />
                    </div>
                    <div class="help-block"></div>
                </div>
                <div class="form-actions">
                    <a href="{{ route('admin.password.request') }}" id="forget-password" class="forget-password">{{ _lang('app.forget_password') }}?</a>
                    <button type="submit" class="btn green pull-right submit-form"> {{ _lang('app.login') }} </button>
                </div>
            </form>
</div>
            <!-- END LOGIN FORM -->
@endsection

@section('scripts')
    <script src="{{url('public/backend/js')}}/login.js" type="text/javascript"></script>
@endsection



