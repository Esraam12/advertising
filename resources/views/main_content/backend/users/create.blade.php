@extends('layouts.backend')

@section('pageTitle',_lang('app.add_user'))

@section('breadcrumb')
<li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>

<li><a href="{{route('users.index')}}">{{_lang('app.users')}}</a> <i class="fa fa-circle"></i></li>

<li><span> {{_lang('app.create')}}</span></li>

@endsection

@section('js')
<script src="{{url('public/backend/js')}}/users.js" type="text/javascript"></script>
@endsection
@section('content')
<form method="" action="" id="addEditUsersForm" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.basic_info') }}</h3>
        </div>
        <div class="panel-body">


            <div class="form-body">
                <input type="hidden" name="id" id="id" value="0">

                <div class="form-group form-md-line-input col-md-4">
                    <input type="text" placeholder="{{_lang('app.name')}}" class="form-control" name="name" id="name">
                    <label for="name">{{_lang('app.name') }}</label>
                    <span class="help-block"></span>
                </div>

                <div class="form-group form-md-line-input col-md-4">
                    <input type="email" placeholder="{{_lang('app.email')}}" class="form-control" name="email" id="email">
                    <label for="email">{{_lang('app.email') }}</label>
                    <span class="help-block"></span>
                </div>

                <div class="form-group form-md-line-input col-md-4">
                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="{{_lang('app.password')}}" name="password" id="password" />
                    <label>{{_lang('app.password')}}</label>
                    <span class="help-block"></span>
                </div>

                        <div class="form-group form-md-line-input col-md-6">
                            <label class="control-label">{{_lang('app.image')}}</label>
                        
                            <div class="image_box">
                                <img src="{{url('no-image.png')}}" width="100" height="80" class="image" />
                            </div>
                            <input type="file" name="image" id="image" style="visibility: hidden">
                            <span class="help-block"></span>
                        </div>

                        <div class="form-group form-md-line-input col-md-2 select">
                            <select class="form-control edited" id="active" name="active">
                                <option value="1">{{ _lang('app.active') }}</option>
                                <option value="0">{{ _lang('app.not_active') }}</option>
                            </select>
                            <label for="status">{{_lang('app.status') }}</label>
                            <span class="help-block"></span>
                        </div>

                        
                    
                
               

            </div>
        </div>
    </div>
     

   
    <div class="panel panel-default">
       <div class="panel-heading">
            <h3 class="panel-title">{{_lang('app.personal_info')}}</h3>
        </div>
        <div class="panel-body">
            <div class="form-body">
                
                <div class="form-group form-md-line-input col-md-2">
                    <input type="number" placeholder="{{_lang('app.age')}}" class="form-control" name="age" id="age">
                    <label>{{_lang('app.age')}}</label>
                    <span class="help-block"></span>
                </div>
                <div class="form-group form-md-line-input col-md-2 select">
                    <select class="form-control edited" id="gender" name="gender">
                            <option value="">{{_lang('app.choose')}}</option>
                            <option value="1">{{_lang('app.male')}}</option>
                            <option value="2">{{_lang('app.female')}}</option>
                    </select>
                    <label for="gender">{{_lang('app.gender') }}</label>
                    <span class="help-block"></span>
                </div> 
                <div class="form-group form-md-line-input col-md-3 select">
                        <select class="form-control edited" id="country" name="country">
                            <option value="">{{_lang('app.choose')}}</option>
                            @foreach ($locations as $location)
                            <option value="{{$location->id}}">{{$location->title}}</option>
                            @endforeach
                        </select>
                        <label for=" country">{{_lang('app.country') }}</label>
                        <span class="help-block"></span>
                </div>
                
                <div class="form-group form-md-line-input col-md-3 select">
                        <select class="form-control edited" id="city" name="city">
                            <option value="">{{_lang('app.choose')}}</option>
                        </select>
                        <label for=" city">{{_lang('app.city') }}</label>
                        <span class="help-block"></span>
                </div>
            </div>
        </div>

    <div class="panel panel-default">
         <div class="panel-heading">
            
        </div>
        <div class="panel-body">
            
        </div>
        <div class="panel-footer text-center">
            <button type="button" class="btn btn-info submit-form">{{_lang('app.save') }}</button>
        </div>
    
    </div>

</form>
<script>
var new_lang = {

};
var new_config = {
    
};

</script>
@endsection