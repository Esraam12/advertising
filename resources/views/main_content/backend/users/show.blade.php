@extends('layouts.backend')

@section('pageTitle', _lang('app.users'))
@section('breadcrumb')
<li><a href="{{url('admin')}}">{{_lang('app.dashboard')}}</a> <i class="fa fa-circle"></i></li>
<li><a href="{{url('admin/users')}}">{{_lang('app.users')}}</a> <i class="fa fa-circle"></i></li>
<li><span> {{ $user->username }}</span></li>

@endsection
@section('js')
<script src="{{url('public/backend/js')}}/users.js" type="text/javascript"></script>
@endsection
@section('content')
<div class="modal fade" id="renewSubscription" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="renewSubscriptionLabel"></h4>
            </div>

            <div class="modal-body">
                <form role="form" id="renewSubscriptionForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" id="id" value="{{$user->id}}">
                    <div class="form-body">
                    


                    </div>


                </form>

            </div>

            <div class="modal-footer">
                <span class="margin-right-10 loading hide"><i class="fa fa-spin fa-spinner"></i></span>
                <button type="button" class="btn btn-info submit-form">{{_lang("app.save")}}</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">{{_lang("app.close")}}</button>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="row">
        <div class="col-md-6">
            <div class="col-md-12">

                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box yellow-crusta">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{{ _lang('app.personal')}}
                        </div>
                      
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-hover">

                                <tbody>
                                    <tr>
                                        <td>{{ _lang('app.username')}}</td>
                                        <td>{{$user->username}}</td>

                                    </tr>
                                    <tr>
                                        <td>{{ _lang('app.email')}}</td>
                                        <td>{{$user->email}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ _lang('app.image')}}</td>
                                        <td><img style="width: 100px;height: 100px;" alt="" src="{{url('public/uploads/users')}}/{{$user->image}}"></td>
                                    </tr>
                                   
                                    
                                </tbody>
                            </table>
                        </div>
                 
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->


            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
        
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{{ _lang('app.additional_info')}}
                        </div>
        
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-hover">
        
                                <tbody>
                                    <tr>
                                        <td>{{ _lang('app.age')}}</td>
                                        <td>{{$user->age}}</td>
                                    </tr>
                                   
                                    @if ($user->gender == 1)
                                    @php $class = 'fa fa-male'; @endphp
                                    @else
                                    @php $class = 'fa fa-female'; @endphp
                                    @endif
                                    <tr>
                                        <td>{{ _lang('app.gender')}}</td>
                                        <td>
                                            <div style="margin-top:2%"><i class="{{$class}} fa-2x"></i></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{ _lang('app.country')}}</td>
                                        <td>{{$user->country}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ _lang('app.city')}}</td>
                                        <td>{{$user->city}}</td>
                                    </tr>
                                    <br>
                                    <br>
        
                                </tbody>
                            </table>
                        </div>
        
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
        
        
            </div>
        </div>
      
    </div>
</div>
@endsection
