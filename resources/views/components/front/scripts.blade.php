<script type="text/javascript" src="{{url('public/front/js')}}/jquery.min.js"></script>
<script type="text/javascript" src="{{url('public/front/js')}}/popper.min.js"></script>
<script type="text/javascript" src="{{url('public/front/js')}}/bootstrap.min.js"></script>
<script src="{{url('public/front/js')}}/wow.min.js"></script>
<script src="{{url('public/front/js')}}/jquery.easing.min.js"></script>
<script src="{{url('public/front/js')}}/slick.min.js"></script>
<script src="{{url('public/front/js')}}/slick-animation.min.js"></script>
<script src="{{url('public/front/js')}}/lightbox.min.js"></script>
<script src="{{url('public/front/js')}}/utilities.js"></script>
<script src="{{url('public/front/js')}}/wSelect.js"></script>
<script src="{{url('public/front/js')}}/main.js"></script>
<script src="{{url('public/front/js')}}/bootstrap-notify.min.js"></script>

<script src="{{url('public/front/js/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{url('public/front/js/additional-methods.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('public/front/js/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/front/js/toastr.js') }}"></script>
<script type="text/javascript" src="{{ url('public/front/js/my.js') }}"></script>
<script type="text/javascript" src="{{ url('public/front/scripts/favourites.js') }}"></script>
@if ($user)
    <script src="//{{ Request::getHost() }}:{{env('LARAVEL_ECHO_PORT')}}/socket.io/socket.io.js"></script>
    <script src="{{ url('public/js/laravel-echo-setup.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ url('public/front/scripts/notifications.js') }}"></script>
@endif

@yield('js')