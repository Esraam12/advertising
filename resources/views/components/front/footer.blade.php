<footer class="mt-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 animated wow fadeInUp">
                <div class="footer-about mb-lg-0 mb-4">
                    <p>{{ str_limit($settings_translations->about_us,250,'......')  }}</p>
                    <div class="social-links">
                        <a href="{{isset($settings['social_media']->facebook) ? $settings['social_media']->facebook : ''}}"><i class="fab fa-facebook-f"></i></a>
                        <a href="{{isset($settings['social_media']->twitter) ? $settings['social_media']->twitter : ''}}"><i class="fab fa-twitter"></i></a>
                        <a href="{{isset($settings['social_media']->youtube) ? $settings['social_media']->youtube : ''}}"><i class="fab fa-youtube"></i></a>
                        <a href="{{isset($settings['social_media']->instagram) ? $settings['social_media']->instagram : ''}}"><i class="fab fa-instagram"></i></a>
                    </div> 
                </div>
            </div>
            <div class="col-lg-3 col-md-6 animated wow fadeInUp">
            <h5>{{_lang('app.useful_links')}}</h5>
                <ul>
                    <li><a href="{{url($lang_code)}}">{{_lang('app.home')}}</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 animated wow fadeInUp">
            <h5>{{_lang('app.what_we_offer')}}</h5>
                <ul>
                    
                </ul>
            </div>
            <div class="col-lg-3 animated wow fadeInUp">
                <div class="contact-footer mt-lg-0 mt-4">
                <h5>{{_lang('app.contact_us')}}</h5>
               
                    <div class="form">
                        <div class="col-xs-12">
                            <div class="header-topbar-col center767">
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fa fa-phone"
                             style=" margin-right:4px;"></i>{{$settings['phone']->value}}</a></li>
                             <li> <a href="mailto:{{$settings['email']->value}}">
                                 <i class="fa fa-envelope" style="margin-right:4px;; "></i>{{$settings['email']->value}}</a></li>
                                </ul>
                            </div>
                        </div>
                    <button class="pink-btn">{{_lang('app.contact_us')}}</button>
                       </a>
                   
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>

<section class="copyrights animated wow fadeInUp">
    <div class="container text-center"><span> Esraa Mostafa © 2020.</span>
        </div>
</section>