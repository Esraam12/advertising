<header>
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                  
                </div>
                <div class="col-md-9">
                    <ul>
                        @if (!Auth::user())
                            <li class="d-none">
                            <a href="{{route('register')}}"><i class="fas fa-user-plus"></i>{{_lang('app.register')}}</a>
                            </li>
                            <li class="d-none">
                                <a href="{{route('login')}}"><i class="fas fa-user"></i>{{_lang('app.login')}}</a>
                            </li>
                        @else
                            <li class="drop">
                                <div class="dropdown show">
                                    <a class="dropdown-toggle" href="{{route('front.user_profile')}}" >
                                        <img src="{{url('public/uploads/users/'.$user->image)}}" class="img-fluid">
                                        <span>{{_lang('app.welcome')}} {{ $user->username }}</span>
                            
                                    </a>
                                    
                                </div>
                            </li>
                            
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
                                        class="fas fa-sign-out-alt" style="font-size:1.5em"></i></a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        @endif
                        <li>
                            <div class="dropdown lang">
                                <button class=" dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-globe"></i>{{_lang('app.language')}}
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @if ($lang_code == 'ar')
                                        <a class="dropdown-item" href="#"><img src="{{url('public/front')}}/images/kuwait-flag-button-1.png">عربي</a>
                                        <a class="dropdown-item" href="{{url($next_lang_code.'/'.substr(Request()->path(), 3) . (request()->getQueryString() ? ('?' . request()->getQueryString()) : '') )}}"><img src="{{url('public/front')}}/images/Britain-512.png">English</a>
                                    @else
                                        <a class="dropdown-item" href="#"><img src="{{url('public/front')}}/images/Britain-512.png">English</a>
                                        <a class="dropdown-item" href="{{url($next_lang_code.'/'.substr(Request()->path(), 3) . (request()->getQueryString() ? ('?' . request()->getQueryString()) : '') )}}"><img src="{{url('public/front')}}/images/kuwait-flag-button-1.png">عربي</a>
                                    @endif
                                    
                                </div>
                            </div>
                        </li>
                        
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <div class="bottom-header">
       
    </div>

</header>
