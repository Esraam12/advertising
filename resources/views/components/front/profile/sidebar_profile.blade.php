<section class="sidebar">
    <div class="profile-sidebar user-sidebar animated wow fadeInUp">
        <img src="{{url('public/uploads/users/'.$user->image)}}" class="img-fluid">
        <p class="name"> {{$user->username}}</p>
        <p class="email">{{$user->email}}</p>
        <ul class="side-nav">
            <li>
                <a href="{{route('front.user_profile')}}"><i class="fas fa-user"></i> {{_lang('app.profile')}}</a>
            </li>
        </ul>
    </div>
</section>