<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
@yield('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>@yield('pageTitle')</title>


<link rel="stylesheet" href="{{url('public/front/css')}}/bootstrap.min.css" media="all" />
<link rel="stylesheet" href="{{url('public/front/css')}}/slick.css" media="all" />
<link rel="stylesheet" href="{{url('public/front/css')}}/slick-theme.css" media="all" />
<link rel="stylesheet" href="{{url('public/front/css')}}/animate.min.css" media="all" />
<link rel="stylesheet" href="{{url('public/front/css')}}/lightbox.min.css" media="all" />
<link rel="stylesheet" href="{{url('public/front/css')}}/all.min.css" media="all" />
<link rel="stylesheet" href="{{url('public/front/css')}}/wSelect.css" media="all" />
<link rel="stylesheet" href="{{url('public/front/css')}}/toastr.css" media="all" />
<link rel="stylesheet" href="{{url('public/front/css')}}/main.css" media="all" />
@if ($lang_code == 'ar')
<link rel="stylesheet" href="{{url('public/front/css')}}/ar-SA/layout.css">
@else
<link rel="stylesheet" href="{{url('public/front/css')}}/en-US/layout.css">
@endif
<link href="https://fonts.googleapis.com/css?family=Roboto|Tajawal&display=swap" rel="stylesheet">

<script>
    window.laravel_echo_port='{{env("LARAVEL_ECHO_PORT")}}';
    var config = {
        url: " {{ _url('') }}",
        base_url: " {{ url('') }}",
        lang_code: "{{ $lang_code }}"  
    };


    var lang = {
       register : "{{_lang('app.register')}}",
       choose : "{{_lang('app.choose')}}",
       login : "{{_lang('app.login')}}",
       verify : "{{_lang('app.verify')}}",
       loading : "{{_lang('app.loading')}}",
       load_more : "{{_lang('app.load_more')}}",
       close : "{{_lang('app.close')}}",
       error : "{{_lang('app.error')}}",
       edit: "{{ _lang('app.edit')}}",
       save: "{{ _lang('app.save')}}",
       add: "{{ _lang('app.add')}}",
       send: "{{ _lang('app.send')}}",
       abuse: "{{ _lang('app.report_abuse')}}",
       delete_confirmation :"{{_lang('app.are_you_sure_that_you_want_to_delete_this_post?')}}",
       yes:"{{_lang('app.yes')}}",
       no:"{{_lang('app.no')}}",
       resend_code: "{{_lang('app.send_the_code_again')}}"
    };
  
        
       
        

  

</script>