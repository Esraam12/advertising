

@if (Permissions::check('users'))
<li class="nav-item {{ session('sidebar.users') ? 'active':'' }}" data-name="users">
    <a href="{{ route('users.index') }}" class="nav-link">
        <i class="icon-users"></i>
        <span class="title">{{ _lang('app.users') }}</span>
    </a>
</li>
@endif


@if (Permissions::check('locations'))
<li class="nav-item {{ session('sidebar.locations') ? 'active':'' }}" data-name="locations">
    <a href="{{ route('locations.index') }}" class="nav-link">
        <i class="icon-pointer"></i>
        <span class="title">{{ _lang('app.locations') }}</span>
    </a>
</li>
@endif
@if (Permissions::check('banners'))
<li class="nav-item {{ session('sidebar.banners') ? 'active':'' }}" data-name="banners">
    <a href="{{ route('banners.index') }}" class="nav-link">
        <i class="icon-pointer"></i>
        <span class="title">{{ _lang('app.banners') }}</span>
    </a>
</li>
@endif
@if (Permissions::check('settings'))
<li class="nav-item {{ session('sidebar.settings') ? 'active':'' }}" data-name="settings">
    <a href="{{ route('settings') }}" class="nav-link">
        <i class="icon-settings"></i>
        <span class="title">{{ _lang('app.settings') }}</span>
    </a>
</li>
@endif



