<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu page-sidebar-menu-light" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->
         
            <li class="nav-item {{ session('sidebar.dashboard') ? 'active':'' }}" data-name="dashboard">
                <a href="{{url('admin')}}" class="nav-link nav-toggle">
                    <i class="icon-screen-desktop"></i><span class="title">{{_lang('app.dashboard')}}</span>
                </a>
            </li>
            @if ($User->type == 1)
                @include('components.backend.admin_side')
            @elseif ($User->type == 2)   
                @include('components.backend.specialist_side')
            @endif
            


        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>