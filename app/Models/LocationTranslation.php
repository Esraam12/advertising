<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LocationTranslation extends MyModel
{
    use SoftDeletes;
    protected $table = "location_translations";
}
