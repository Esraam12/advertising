<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends MyModel {
    use SoftDeletes;
    protected $table = "locations";

    protected $casts = [
        'id' => 'integer'
    ];

    public static $sizes = array(
        's' => array('width' => 16, 'height' => 16),
        'm' => array('width' => 400, 'height' => 400)
    );

    public static function getAll($parent_id = 0) {
        return static::join('location_translations as trans', 'locations.id', '=', 'trans.location_id')
                        ->orderBy('locations.this_order', 'ASC')
                        ->where('locations.parent_id',$parent_id)
                        ->where('trans.locale', static::getLangCode())
                        ->select('locations.*','trans.title')
                        ->get();
    }

    public function childs() {
        return $this->hasMany(Location::class, 'parent_id');
    }

    public function translations() {
        return $this->hasMany(LocationTranslation::class, 'location_id');
    }


   public static function transform($item)
   {
       
       $transformer = new \stdClass();
       $transformer->id = $item->id;
       $transformer->title = $item->title;
       if ($item->parent_id == 0) {
            $transformer->dial_code = $item->dial_code;
            $transformer->image = url('public/uploads/locations') . '/' . str_replace('s_', 'm_', $item->image);
       }
       if ($item->childrens) {
          $transformer->childrens = $item->childrens;
       }
       return $transformer;
   }

    public static function transformFront($item)
    {
        $transformer = new \stdClass();
        $transformer->id = $item->id;
        $transformer->title = $item->title;
        if ($item->parent_id == 0) {
            $transformer->dial_code = $item->dial_code;
            $transformer->image = url('public/uploads/locations') . '/' . $item->image;
        }
        return $transformer;
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($location) {
            foreach ($location->translations as $translation) {
                $translation->delete();
            }
            foreach ($location->childs as $child) {
                $child->delete();
            }
            
        });

        static::deleted(function($location) {
            self::deleteUploaded('locations', $location->image);
        });
    }

}
