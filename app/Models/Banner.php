<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends MyModel 
{
    protected $table = "banners";
    public function location()
    {
        return $this->belongsTo(Location::class,'location_id');
    }
    public function translations() {
        return $this->hasMany(BannerTranslations::class, 'banner_id');
    }
    protected static function boot() {
        parent::boot();

        static::deleting(function($banner) {
            foreach ($banner->translations as $translation) {
                $translation->delete();
            }
        });

        static::deleted(function($banner) {
            self::deleteUploaded('banners', $banner->image);
        });
    }
}
