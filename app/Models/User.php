<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\ModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasOne;

class User extends Authenticatable {
    use SoftDeletes;
    use Notifiable;
    use ModelTrait;

    protected $casts = array(
        'id' => 'integer',
        'age' => 'float'
    );

    public static $gender = [
        'male' => 1,
        'female' => 2
    ];
    public static $sizes = array(
        's' => array('width' => 120, 'height' => 120),
        'm' => array('width' => 400, 'height' => 400),
    );
    
    public function packages()
    {
        return $this->hasMany(PackageSubscription::class, 'user_id');
    }

    public function location()
    {
        return $this->belongsTo(Location::class,'location_id');
    }

    
    public static function getUserInfo($user)
    {
      $lang_code = static::getLangCode();
       $user = User::join('locations as city','users.location_id','=','city.id')
                                        ->join('location_translations as city_translation',function($query) use($lang_code){
                                            $query->on('city_translation.location_id','=', 'city.id')
                                            ->where('city_translation.locale', $lang_code);
                                        })
                                        ->join('locations as country','city.parent_id','=', 'country.id')
                                        ->join('location_translations as country_translation',function($query) use ($lang_code) {
                                            $query->on('country_translation.location_id','=', 'country.id')
                                                ->where('country_translation.locale', $lang_code);
                                        })
                                        ->where('users.id', $user->id)
                                        ->select(
                                            'users.*',
                                            'country.id as country_id',
                                            'city_translation.title as city',
                                            'country_translation.title as country'
                                        )
                                        ->first(); 
        return $user;
    }
    public static function transform($item)
    {
        $transformer = new \stdClass();
        $transformer->username = $item->username;
        $transformer->email = $item->email;
        $transformer->dial_code = $item->dial_code;
        $transformer->mobile = $item->mobile;
        $transformer->image = url('public/uploads/users') . '/' . $item->image;
        $transformer->age = $item->age;
        $transformer->gender = $item->gender;
        if ($item->gender == 1) {
            $transformer->gender_text = _lang('app.male');   
        }else{
            $transformer->gender_text = _lang('app.female');   
        }
        $transformer->city_id = $item->location_id;
        $transformer->city = $item->city;
        $transformer->country_id = $item->country_id;
        $transformer->country = $item->country;
        return $transformer;
    }

   
    
    protected static function boot() {
        parent::boot();
        
        static::deleted(function($user) {
            if ($user->user_image != 'default.png') {
                User::deleteUploaded('users',$user->image);
            }
        });
    }
   

}
