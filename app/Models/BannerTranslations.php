<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannerTranslations extends MyModel 
{
    protected $table = "banner_translations";
}
