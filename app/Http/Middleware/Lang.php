<?php

namespace App\Http\Middleware;

use Closure, Config, App, Redirect;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->segment(1);
        $locales = Config::get('app.locales');
        
        if (!in_array($locale, $locales)) {
            $segments = $request->segments();
            $segments = array_prepend($segments, Config::get('app.fallback_locale'));
            return Redirect::to(implode('/', $segments));
        }
        App::setLocale($locale);
        return $next($request);
    }
}
