<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use DB;
use Validator;
use App\Models\Banner;
use App\Models\BannerTranslations;
use App\Models\Location;

class BannersController extends BackendController
{
    private $rules = array(
        'active'       => 'required',
        'this_order'       => 'required',
        'image' => 'required|image|mimes:gif,png,jpeg|max:1000',
        
    );
    public function __construct()
    {
        parent::__construct();
        $this->middleware('CheckPermission:banners,open', ['only' => ['index']]);
        $this->middleware('CheckPermission:banners,add', ['only' => ['store']]);
        $this->middleware('CheckPermission:banners,edit', ['only' => ['show', 'update']]);
        $this->middleware('CheckPermission:banners,delete', ['only' => ['delete']]);
    }
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->_view('banners/index', 'backend');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['locations'] = Location::transformCollection(Location::getAll());
        return $this->_view('banners/create', 'backend');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $columns_arr = array(
            'title' => 'required|unique:banner_translations,title',
            
        );
        $lang_rules = $this->lang_rules($columns_arr);
        $this->rules = array_merge($this->rules, $lang_rules);
        $validator = Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return _json('error', $errors);
        }
        DB::beginTransaction();
        try {

            $banner= new Banner;
            $banner->active = $request->input('active');
            $banner->this_order = $request->input('this_order');
            $banner->duration = $request->input('duration');
            $banner->frequency = $request->input('frequency');
            $banner->image =Banner::upload($request->file('image'), 'banners', true);
            $banner->location_id = $request->input('city');
            $banner->save();

            $this->createbannerTranslations($banner->id, $request);
           
           
            DB::commit();
            return _json('success', _lang('app.added_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }
    public function edit($id)
    {
        $find = banner::find($id);
        if (!$find) {
            return $this->err404();
        }
        $this->data['banner'] = $find;
        $this->data['city'] = Location::where('id', $this->data['banner']->location_id)->first();

        $this->data['locations'] = Location::getAll();
        $this->data['cities'] = Location::getAll($this->data['city']->parent_id);
        $this->data['translations'] = BannerTranslations::where('banner_id', $id)->get()->keyBy('locale');
        return $this->_view('banners/edit', 'backend');
    }


    public function update(Request $request, $id)
    {
        $banner= Banner::find($id);
        if (!$banner) {
            return _json('error', _lang('app.error_is_occured'), 404);
        }

        $this->rules['image'] = 'image|mimes:gif,png,jpeg|max:1000';

        $columns_arr = array(
            'title' => 'required|unique:banner_translations,title,' . $id . ',banner_id',
            
    
        );

        $lang_rules = $this->lang_rules($columns_arr);
        $this->rules = array_merge($this->rules, $lang_rules);
        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return _json('error', $errors);
        }
        DB::beginTransaction();
        try {

            $banner->active = $request->input('active');
            $banner->this_order = $request->input('this_order');
            $banner->duration = $request->input('duration');
            $banner->frequency = $request->input('frequency');
            $banner->location_id = $request->input('city');

            if ($request->file('image')) {
                banner::deleteUploaded('banner', $banner->image);
                $banner->image = Banner::upload($request->file('image'), 'banner', true);
            }

            $banner->save();

            bannerTranslations::where('banner_id', $banner->id)->forceDelete();
            $this->createbannerTranslations($banner->id, $request);
            DB::commit();
            return _json('success',  _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error', $ex->getMessage(), 400);
        }
    }
    public function destroy($id)
    {
        $banner= banner::find($id);
        if (!$banner) {
            return _json('error', _lang('app.error_is_occured'), 404);
        }
        DB::beginTransaction();
        try {
            $banner->delete();
            DB::commit();
            return _json('success', _lang('app.deleted_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            if ($ex->getCode() == 23000) {
                return _json('error', _lang('app.this_record_can_not_be_deleted_for_linking_to_other_records'), 400);
            } else {
                return _json('error', _lang('app.error_is_occured'), 400);
            }
        }
    }
    private function createbannerTranslations($banner_id, $request)
    {
        $banner_translations = array();
        $title = $request->input('title');
        $description = $request->input('description');
       
        foreach ($title as $key => $value) {
            $banner_translations[] = array(
                'locale' => $key,
                'title' => $value,
                'description'=> $description[$key],
                'banner_id' => $banner_id
            );
        }
        BannerTranslations::insert($banner_translations);
    }
    public function data(Request $request)
    {
        $banner = Banner::Join('banner_translations', 'banners.id', '=', 'banner_translations.banner_id')
            ->where('banner_translations.locale', $this->lang_code)
            ->select([
                'banners.id', 'banners.image', "banner_translations.title", "banners.active", "banners.duration"
            ]);
        return \DataTables::eloquent($banner)
            ->addColumn('options', function ($item) {

                $back = "";
                if (\Permissions::check('banners', 'edit') || \Permissions::check('banners', 'delete')) {
                    $back .= '<div class="btn-group">';
                    $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">' . _lang('app.options');
                    $back .= '<i class="fa fa-angle-down"></i>';
                    $back .= '</button>';
                    $back .= '<ul class = "dropdown-menu" role = "menu">';
                    if (\Permissions::check('banners', 'edit')) {
                        $back .= '<li>';
                        $back .= '<a href="' . route('banners.edit', $item->id) . '">';
                        $back .= '<i class = "icon-docs"></i>' . _lang('app.edit');
                        $back .= '</a>';
                        $back .= '</li>';
                    }

                    if (\Permissions::check('banners', 'delete')) {
                        $back .= '<li>';
                        $back .= '<a href="" data-toggle="confirmation" onclick = "Banner.delete(this);return false;" data-id = "' . $item->id . '">';
                        $back .= '<i class = "icon-docs"></i>' . _lang('app.delete');
                        $back .= '</a>';
                        $back .= '</li>';
                    }

                    $back .= '</ul>';
                    $back .= ' </div>';
                }
                return $back;
            })
            ->addColumn('active', function ($item) {
                if ($item->active == 1) {
                    $message = _lang('app.active');
                    $class = 'label-success';
                } else {
                    $message = _lang('app.not_active');
                    $class = 'label-danger';
                }
                $back = '<span class="label label-sm ' . $class . '">' . $message . '</span>';
                return $back;
            })
            ->editColumn('image', function ($item) {
                $back = '<img src="' . url('public/uploads/banners/' . $item->image) . '" style="height:100px;width:400px;"/>';
                return $back;
            })
            ->escapeColumns([])
            ->make(true);
    }
}
