<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Models\Admin;
use DB;
use App\Models\Specialist;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;

class ProfileController extends BackendController {

   

    private $specialist_rules = array(
        'old_password' => 'required',
        'password' => 'required|min:6',
        'confirm_password' => 'required|same:password'
    );

    public function index() {
        return $this->_view('profile/index', 'backend');
    }


    public function update(Request $request) {
        try {
            if ($this->User->type == 1) {
                $rules = array(
                    'username' => 'required|unique:admins,username,' . $this->User->id,
                    'email' => 'required|email|unique:admins,email,' . $this->User->id,
                    'phone' => 'required|numeric|unique:admins,phone,' . $this->User->id
                );
            }else if ($this->User->type == 2) {
                $rules = $this->specialist_rules;
            }
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $errors = $validator->errors()->toArray();
                return _json('error', $errors);
            } 
            if ($this->User->type == 1) {
                $this->User->username = $request->input('username');
                $this->User->email = $request->input('email');
                $this->User->phone = $request->input('phone');
                if ($request->input('password') !== null) {
                    $this->User->password = bcrypt($request->input('password'));
                }
            }
            if ($this->User->type == 2) {
               if (!password_verify($request->input('old_password'), $this->User->password)) {
                   return _json('error',_lang('app.old_password_is_not_correct'),400);
               }
                $this->User->password = bcrypt($request->input('password'));
              }
                $this->User->save();
                return _json('success', _lang('app.updated_successfully'));
            } catch (Exception $ex) {
                return _json('error', _lang('app.error_is_occured'));
            }
        }
}


