<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\User;
use App\Models\Package;
use App\Models\Session;
use App\Models\Specialist;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;


class AdminController extends BackendController {

    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request) {
        try {
      
        $from = $request->input('from');
        $to = $request->input('to');

        if ($request->all()) {
            foreach ($request->all() as $key => $value) {
                if ($value) {
                    $this->data[$key] = $value;
                }
            }
        }

                                   
        return $this->_view('index', 'backend');
        } catch (\Exception $ex) {
            return $this->err404();
        }
    }


    public function error() {
        return $this->_view('err404','backend');
    }



}
