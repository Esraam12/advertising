<?php

namespace App\Http\Controllers\Admin;

use DB;

use Validator;
use App\Models\User;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;

class UsersController extends BackendController {

    private $rules = array(
        'name' => 'required',
        'email' => 'required|unique:users',
        'age' => 'required',
        'password' => 'required',
        'gender' => 'required',
        'country' => 'required',
        'city' => 'required',
        'active' => 'required',
        'image' => 'image|mimes:jpg,png,jpeg'
    );  
    public function __construct() {

        parent::__construct();
        $this->middleware('CheckPermission:users,open', ['only' => ['index']]);
        $this->middleware('CheckPermission:users,add', ['only' => ['create','store']]);
        $this->middleware('CheckPermission:users,edit', ['only' => ['edit', 'update']]);
        $this->middleware('CheckPermission:users,delete', ['only' => ['delete']]);
    }

    public function index(Request $request) {
        return $this->_view('users/index', 'backend');
    }
    public function create(Request $request) {
        $this->data['locations'] = Location::transformCollection(Location::getAll());
        return $this->_view('users/create', 'backend');
    }
    public function store(Request $request) {

        $validator = Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return _json('error', $errors);
        }
        DB::beginTransaction();
        try {
                $user = new User;
                $user->username = $request->input('name');
                $user->email = $request->input('email');
                if ($request->file('image')) {
                    $user->image = User::upload($request->file('image'), 'users', true);
                }else{
                    $user->image = 'default.png';
                }
                $user->age = $request->input('age');
                $user->password = bcrypt($request->input('password'));
                $user->gender = $request->input('gender');
                $user->location_id = $request->input('city');
                $user->save();
                DB::commit();
                return _json('success', _lang('app.added_successfully'));
        } catch (\Exception $ex) {
            DB::rollback();
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }
    
 
    public function status($id){
        try {
            $user = User::find($id);
            $user->active = !$user->active;
            $user->save();
            return _json('success', _lang('app.success'));
        } catch (\Exception $e) {
            return _json('error', _lang('app.error_is_occured'));
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id) {
        $user = User::join('locations as city','city.id','=','users.location_id')
                    ->join('locations as country', 'country.id', '=', 'city.parent_id')
                    ->join('location_translations as city_trans',function($query){
                        $query->on('city_trans.location_id','=', 'city.id')
                        ->where('city_trans.locale',$this->lang_code);
                    })
                    ->join('location_translations as country_trans', function ($query) {
                        $query->on('country_trans.location_id', '=', 'country.id')
                            ->where('country_trans.locale', $this->lang_code);
                    })
                    ->select('users.*', 'country_trans.title as country', 'city_trans.title as city')
                    ->where('users.id',$id)
                    ->first();
        if (!$user) {
            return $this->err404();
        } 
        $this->data['user'] = $user;
        return $this->_view('users/show', 'backend');
    }
    public function edit($id)
    {
        try {
            $find = User::find($id);
            if (!$find) {
                return $this->err404();
            }
            $this->data['user'] = $find;
            $this->data['city'] = Location::where('id', $this->data['user']->location_id)->first();

            $this->data['locations'] = Location::getAll();
            $this->data['cities'] = Location::getAll($this->data['city']->parent_id);
            return $this->_view('users/edit', 'backend');
        } 
        catch (\Exception $ex){
            return $this->err404();
        }
    }
   

    public function update(Request $request, $id)
    {
        if ($request->input('password') === null) {
            unset($this->rules['password']);
        }
        $user= User::find($id);
        if (!$user) {
            return _json('error', _lang('app.error_is_occured'), 404);
        }
        $this->rules['password'] = 'min:6';
        $this->rules['email']= 'required|email|max:255|unique:users,email,'.$id.',id';

        $validator = Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return _json('error', $errors);
        }
        try {
           
            $user->username = $request->input('name');
            $user->email = $request->input('email');      
            $user->age = $request->input('age');
            if ($request->input('password') !== null) {
                $user->password = bcrypt($request->input('password'));
            }
            $user->gender = $request->input('gender');
            $user->location_id = $request->input('city');
            if ($request->file('image')) {
                if ($user->image != 'default.png') {
                    User::deleteUploaded('users',$user->image);
                }
                $user->image = User::upload($request->file('image'), 'users', true);
            } 
            $user->save(); 
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            return _json('error', _lang('app.error_is_occured'), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id) {
        $user = User::find($id);
        if ($user == null) {
            return _json('error', _lang('app.error_is_occured'));
        }
        try {
            $user->delete();
            return _json('success', _lang('app.deleted_successfully'));
        } catch (\Exception $ex) {
            if ($ex->getCode() == 23000) {
                return _json('error', _lang('app.this_record_can_not_be_deleted_for_linking_to_other_records'), 400);
            } else {
                return _json('error', _lang('app.error_is_occured'), 400);
            }
        }
    }



    public function data(Request $request) {
        $users = User::select(['id','username', 'email','active','image','gender']);

        return \DataTables::eloquent($users)
                ->addColumn('options', function ($item){
                    $js='Users';
                    $back = "";
                          
                        if (\Permissions::check('articles', 'edit') ||\Permissions::check('users', 'open')) {
                            $back .= '<div class="btn-group">';
                            $back .= ' <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> options';
                            $back .= '<i class="fa fa-angle-down"></i>';
                            $back .= '</button>';
                            $back .= '<ul class = "dropdown-menu" role = "menu">';
                            if (\Permissions::check('users', 'open')) {
                                $back .= '<li>';
                                $back .= '<a href="'.route('users.show',$item->id).'" onclick = "" data-id = "' . $item->id . '">';
                                $back .= '<i class = "icon-docs"></i>' . _lang('app.show');
                                $back .= '</a>';
                                $back .= '</li>';
                            }
                            if (\Permissions::check('users', 'edit')) {
                                $back .= '<li>';
                                $back .= '<a href="' . route('users.edit', $item->id) . '">';
                                $back .= '<i class = "icon-docs"></i>' . _lang('app.edit');
                                $back .= '</a>';
                                $back .= '</li>';
                            }
                            $back .= '</ul>';
                            $back .= ' </div>';
                        }
                    return $back;
                })
                ->editColumn('active', function ($item) {
                    if ($item->active == 1) {
                        $message = _lang('app.active');
                        $class = 'btn-info';
                    } else {
                        $message = _lang('app.not_active');
                        $class = 'btn-danger';
                    }
                    $back = '<a class="btn ' . $class . '" onclick = "Users.status(this);return false;" data-id = "' . $item->id . '" data-status = "' . $item->active . '">' . $message . ' <a>';
                    return $back;
                })
                ->editColumn('gender', function ($item) {
                    if ($item->gender == User::$gender['male']) {
                        $class = 'fa fa-male';
                    } else {
                        $class = 'fa fa-female';
                    }
                    $back = '<div style="margin-top:2%"><i class="' . $class . ' fa-2x"></i></div>';
                    return $back;
                }) 
                ->editColumn('image', function ($item) {
                   if (!$item->image) {
                       $item->image = 'default.png';
                   }
                    $back = '<img src="' . url('public/uploads/users/' . $item->image) . '" style="height:50px;width:50px;"/>';
                    return $back;
                })
                ->escapeColumns([])
                ->make(true);
    }

   
}
