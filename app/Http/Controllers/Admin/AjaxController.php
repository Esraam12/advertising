<?php

namespace App\Http\Controllers\Admin;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;

class AjaxController extends BackendController {

    public function change_lang(Request $request) {
        $lang_code = $request->input('lang_code');
        $long = 7 * 60 * 24;
        return response()->json([
                    'type' => 'success',
                    'message' => $lang_code
                ])->cookie('AdminLang', $lang_code, $long);
    }

    public function change_sidebar_state(Request $request) {
        $request->session()->forget('sidebar');
        $data = json_decode($request->getContent());
        foreach ($data as $item) {
            $request->session()->put('sidebar.' . $item, 1);
        }
        return _json('success',$data);
    }

    public function getLocations($country_id)
    {
        $locations = Location::getAll($country_id)->toArray();
        return _json('success', $locations);
    }


 
   
    public function deleteImage(Request $request)
    {
        try {
            $column_name = $request->input('col');

            // for edit form for non uploaded images
            if (!$request->input('model')) {
                return _json('success', _lang('app.updated_successfully'));
            }
            //

            $model = "App\Models\\" . $request->input('model');
            $model::deleteUploaded($request->input('folder'), $request->input('image'));

            $find = $model::find($request->input('id'));
            if (!json_decode($find->$column_name,true)) {
                $find->$column_name = "";
            } else {
                $images = json_decode($find->$column_name,true);
                $image_index = array_search($request->input('image'), $images);
                
                if (isset($images[$image_index])) {
                    unset($images[$image_index]); 
                }
                if (count($images) == 0) {
                    $find->$column_name = "";
                }else{
                    //$find->$column_name = json_encode(array_values($images));
                    $find->$column_name = json_encode($images, JSON_FORCE_OBJECT);
                }
            }
            $find->save();
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            return _json('error', _lang('app.error_is_occured'), 400);
        }
       

    }

}
