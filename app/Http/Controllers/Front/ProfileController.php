<?php

namespace App\Http\Controllers\Front;

use DB;
use Validator;
use App\Models\User;
use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FrontController;

class ProfileController extends FrontController
{
    private $rules = array(
        'name' => 'required',
        'email' => 'required|unique:users',
        'age' => 'required',
        'password' => 'required',
        'gender' => 'required',
        'country' => 'required',
        'city' => 'required',
        'image' => 'image|mimes:jpg,png,jpeg'
    );
    public function __construct()
    {
        parent::__construct();
    }
    public function show(){
        try{
                $user = $this->user;
                $this->data['user'] = User::getUserInfo($user);
                return $this->_view('profile.show');
        }catch (\Exception $e) {
            return  $this->error404();
        }

    }
    public function edit(){
        try{
                $user = $this->user;
                $this->data['user'] = User::join('locations', 'users.location_id', '=', 'locations.id')
                                            ->where('users.id', $user->id)
                                            ->select(
                                                'users.*',
                                                'locations.parent_id as country_id'
                                            )
                                            ->first(); 
                $this->data['locations'] = Location::getAll();
                $this->data['cities'] = Location::getAll($this->data['user']->country_id);
                return $this->_view('profile.edit');
            }catch (\Exception $ex) {
                return  $this->error404();
            }

    }
    public function update(Request $request){
        $user= $this->user;
        if ($request->input('password') === null) {
            unset($this->rules['password']);
        }
        $this->rules['password'] = 'min:6';
        $this->rules['email']= 'required|email|max:255|unique:users,email,'. $user->id.',id';
        $validator = Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            $errors = $validator->errors()->toArray();
            return _json('error', $errors);
        }
        try {
           
            $user->username = $request->input('name');
            $user->email = $request->input('email');    
            $user->age = $request->input('age');
            $user->gender = $request->input('gender');
            $user->location_id = $request->input('city');
            if ($request->file('image')) {
                if ($user->image != 'default.png') {
                    User::deleteUploaded('users',$user->image);
                }
                if ($request->input('password') !== null) {
                    $user->password = bcrypt($request->input('password'));
                }
                $user->image = User::upload($request->file('image'), 'users', true);
            } 
            $user->save(); 
            return _json('success', _lang('app.updated_successfully'));
        } catch (\Exception $ex) {
            return _json('error', _lang('app.error_is_occured'),400);
        }

    }
      


}
