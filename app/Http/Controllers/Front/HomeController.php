<?php

namespace App\Http\Controllers\Front;


use Illuminate\Http\Request;
use App\Http\Controllers\FrontController;
use App\Models\User;
use App\Models\Location;
use App\Models\Banner;
class HomeController extends FrontController {

    public function __construct() {
       
        parent::__construct();
       
    }

    public function index(Request $request) {
        $user = $this->user;
        if($user){
            $lang_code = $this->lang_code;
            $user_info = User::getUserInfo($user);
            $user_location=$user_info->location_id;
            $this->data['banners'] = Banner::join('banner_translations', 'banners.id', '=', 'banner_translations.banner_id')
                ->where('banner_translations.locale', $this->lang_code)
                ->where('banners.location_id',$user_location)
                ->where('banners.active', true)
                ->select('banners.image', 'banners.this_order','banners.duration','banners.frequency', 'banner_translations.title', 'banner_translations.description')
                ->orderBy('banners.this_order')
                ->get();
        }
        
        return $this->_view('index');
    }


   



}
