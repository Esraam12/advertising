<?php

namespace App\Http\Controllers\Front\Auth;


use DB;
use App\Models\User;
use App\Models\Package;
use App\Models\Currency;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Models\PackageSubscription;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use App\Events\SuccessfullSubscription;
use App\Http\Controllers\FrontController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends FrontController {
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

use RegistersUsers;

    protected $redirectTo = '/login';

    private $rules = array(
        'name' => 'required',
        'email' => 'required|unique:users',
        'age' => 'required',
        'gender' => 'required',
        'country' => 'required',
        'city' => 'required',
    );

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->middleware('guest');
    }

    public function showRegistrationForm() {
        $this->data['locations'] = Location::transformCollection(Location::getAll(),'Front');
        return $this->_view('auth.register');
    }

    public function register(Request $request) {

        $validator = Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            $this->errors = $validator->errors()->toArray();
            return _json('error', $this->errors);
        }
            try {
                DB::beginTransaction();
                $user = $this->createUser($request->all());
                if (!$user) {
                    return _json('error', _lang('app.error_is_occured'), 400);
                }
                $message = _lang('app.registered_done_successfully');
                DB::commit();
                return _json('success', $message);
            } catch (\Exception $ex) {
                DB::rollback();
                $message = _lang('app.error_is_occured');
                return _json('error', $message, 400);
            }
        
    }

    protected function guard() {
        return Auth::guard('web');
    }


    private function createUser($data)
    {
       
        $user = new User;

        $user->username = $data['name'];
        $user->email = $data['email'];
        $password=$data['password'];
        $user->password =bcrypt($password);
        $user->image = 'default.png';
        $user->age = $data['age'];
        $user->gender = $data['gender'];
        $user->location_id = $data['city'];
        $user->save();
        return $user;
    }


}
