<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\FrontController;
use App\Models\Location;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Auth;
use Validator;

class LoginController extends FrontController {

    use AuthenticatesUsers;

    private $rules = array(
        'username' => 'required',
        'password' => 'required'
    );

   

    public function __construct() {
        parent::__construct();
        $this->middleware('guest', ['except' => ['logout']]);
    }

    public function showLoginForm() {
        $this->data['locations'] = Location::transformCollection(Location::getAll());
        return $this->_view('auth.login');
    }

    public function login(Request $request) {
        try {
            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                if ($request->ajax()) {
                    $errors = $validator->errors()->toArray();
                    return _json('error', $errors);
                } else {
                    return redirect()->back()->withInput($request->all())->withErrors($validator->errors()->toArray());
                }
            }
            $username=$request->input('username');
            $password=$request->input('password');
            $user = $this->checkAuth($username);
            $is_logged_in = false;
            if ($user) {
                if (password_verify($password, $user->password)) {
                    $is_logged_in = true;
                }
            }
            if ($is_logged_in) {
                Auth::guard('web')->login($user);
                if ($request->ajax()) {
                    return _json('success',  route('home'));
                } else {
                    return redirect()->intended( route('home'));
                }
            } else {
                $msg = _lang('messages.invalid_credentials');
                if ($request->ajax()) {
                    return _json('error', $msg);
                } else {
                    return redirect()->back()->withInput($request->only('username', 'remember'))->withErrors(['msg' => $msg]);
                }
            }
        } catch (\Exception $e) {
            $message = _lang('app.something_went_wrong,_try_again_later');
            if ($request->ajax()) {
                return _json('error', $message);
            }
            return redirect()->back()->withInput($request->all())->withErrors(['message' => $message]);;
        }
    }
    public function logout() {
        Auth::guard('web')->logout();
        return redirect()->route('login');
    }

    private function checkAuth($username) {
        $user = User::where('username', $username)
                    ->where('active',true)
                    ->first();
        return $user;
    }

}
