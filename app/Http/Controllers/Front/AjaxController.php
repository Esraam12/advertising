<?php

namespace App\Http\Controllers\Front;

use App\Models\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\FrontController;
use Validator;


class AjaxController extends FrontController {

    public function __construct() {
        parent::__construct();
    }
   
    public function getLocations($country_id)
    {
        $locations = Location::getAll($country_id)->toArray();
        return _json('success', $locations);
    }

   

   

    

   
}
