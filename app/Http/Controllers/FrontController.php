<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Traits\Basic;
use App\Models\Setting;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Models\SettingTranslation;

class FrontController extends Controller {

    use Basic;

    protected $lang_code;
    protected $user = false;
    protected $isUser = false;
    protected $limit = 10;
    protected $data = array();
    

    public function __construct() {
       
        $this->getLangCode();
        $this->getSettings();
        $this->data['page_title'] = '';

        if (Auth::guard('web')->user() != null) {
            $this->user = Auth::guard('web')->user();
            $this->isUser = true;
         ;
        }
        $this->data['user'] = $this->user;
        $this->data['isUser'] = $this->isUser;
    }

    private function getLangCode() {
        $this->lang_code = app()->getLocale();
        $this->data['lang_code'] = $this->lang_code;
        session()->put('lang_code', $this->lang_code);
      
        if ($this->data['lang_code'] == 'ar') {
            $this->data['next_lang_code'] = 'en';
            $this->data['next_lang_text'] = 'English';
        } else {
            $this->data['next_lang_code'] = 'ar';
            $this->data['next_lang_text'] = 'العربية';
        }
    }

    private function getSettings() {
       
        $this->data['settings'] = Setting::get()->keyBy('name');
        $this->data['settings']['social_media'] = json_decode($this->data['settings']['social_media']->value);
        $this->data['settings_translations'] = SettingTranslation::where('locale', $this->lang_code)->first();
    }




    private function buildTree($elements, $model, $parentId = 0)
    {

        $branch = array();
        foreach ($elements as $element) {
            if ($element->parent_id == $parentId) {
                $children = array();
                $children = $this->buildTree($elements, $model, $element->id);
                if ($children) {
                    $element['childrens'] = $children;
                }
                $branch[] = $model::transformFront($element);
            }
        }
        return $branch;
    }

    /** ------------------------------------------------------------------------------------ */
    
    /** ------------------------------------------------------------------------------------ */
    /** ------------------------------------------------------------------------------------ */

    /** ------------------------------------------------------------------------------------ */


    protected function err404($code = false, $message = false) {
        if (!$message) {
            $message = _lang('app.page_not_found');
        }
        if (!$code) {
            $code = 404;
        }
        $this->data['code'] = $code;
        $this->data['message'] = $message;
        return $this->_view('err404');
    }

}
