<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Session;
use App\Events\NotifyFront;
use App\Models\Notification;
use Illuminate\Console\Command;
use App\Events\SessionStatusChanged;

class SessionReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'session:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remind users with session time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $carbon_date = Carbon::now()->format('Y-m-d H:i');
        $sessions = Session::where('type',Session::$types['live'])
            ->whereRaw("DATE_FORMAT(sessions.date, '%Y-%m-%d %H:%i') = '$carbon_date'")
            ->get();
        if ($sessions->count() > 0) {
           foreach ($sessions as $session) {
                $translations = Session::join('session_translations', 'sessions.id', '=', 'session_translations.session_id')
                    ->where('sessions.id', $session->id)
                    ->select('session_translations.locale', 'session_translations.title', 'sessions.category_id')
                    ->get();
                $session_translations = $translations->pluck('title', 'locale')->toArray();
                    

                //sending public notifications to all users [only users]
                event(new SessionStatusChanged($session->id, Notification::$notification_types['started_session'], $session_translations));
                
                event(new NotifyFront([
                    'title' => $session_translations,
                    'category_id' => $translations[0]->category_id,
                    'type' => Session::$types['live']
                ], Notification::$notification_types['started_session']));
           }
        }
    }
}
