<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\EntityAddition' => [
            'App\Listeners\NotifyAllUsers',
        ],
        'App\Events\SessionStatusChanged' => [
            'App\Listeners\NotifyAllUsers',
        ],
        'App\Events\SuccessfullSubscription' => [
            'App\Listeners\NotifyUser',
        ],
        'App\Events\QuestionAnswered' => [
            'App\Listeners\NotifyUser',
        ],
        'App\Events\ActionToBeInformed' => [
            'App\Listeners\InformByEmail',
        ]
        
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
